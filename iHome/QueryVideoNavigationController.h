//
//  QueryVideoNavigationController.h
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"

@interface QueryVideoNavigationController : UINavigationController
@property(retain)DeviceObject *device;
@end
