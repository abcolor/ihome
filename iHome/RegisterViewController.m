//
//  RegisterViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "RegisterViewController.h"
#import "IndicatorObject.h"
#import "WebService.h"
#import "UserPreferencesObject.h"

@interface RegisterViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPasswordAgain;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@end

@implementation RegisterViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.txtUserName.delegate = self;
    self.txtPassword.delegate = self;
    self.txtPassword.secureTextEntry = YES;
    self.txtPasswordAgain.delegate = self;
    self.txtPasswordAgain.secureTextEntry = YES;
    self.txtEmail.delegate = self;
    self.txtPhoneNumber.delegate  =self;
    self.txtMobileNumber.delegate = self;
    self.txtAddress.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(BOOL)checkUserRegisterData {

    if(![self.txtEmail.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入電子郵件" Message:nil];
        return NO;
    }
    else if(![self.txtUserName.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入使用者名稱" Message:nil];
        return NO;
    }
    else if(![self.txtPhoneNumber.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入電話號碼" Message:nil];
        return NO;
    }
    else if(![self.txtMobileNumber.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入手機號碼" Message:nil];
        return NO;
    }
    else if(![self.txtPassword.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入密碼" Message:nil];
        return NO;
    }
    else if(![self.txtPasswordAgain.text length]) {
        [IndicatorObject showDialogWithTitle:@"請再次輸入密碼" Message:nil];
        return NO;
    }
    else if([self.txtPassword.text isEqualToString:self.txtPasswordAgain.text]==NO) {
        [IndicatorObject showDialogWithTitle:@"密碼驗證不正確" Message:nil];
        return NO;
    }
    else if(![self.txtAddress.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入地址" Message:nil];
        return NO;
    }
    
    return YES;
}


#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    [UIView animateWithDuration:0.4f animations:^{
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
    } ];
    return YES;
}

#pragma mark - IBActions


- (IBAction)txtAddressDidBeginEdit:(id)sender {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    [UIView animateWithDuration:0.4f animations:^{
        self.view.frame = CGRectMake(0, -216, screenSize.width, screenSize.height);
    } ];
}


- (IBAction)pressBtnRegister:(id)sender {
    if([self checkUserRegisterData]) {
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        [UIView animateWithDuration:0.4f animations:^{
            self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
        } completion:^(BOOL finished){
            // display wait indication
            IndicatorObject *waitIndicator = [[IndicatorObject alloc] initWithView:self.view];
            [waitIndicator show];
            
            WebServiceCompletionBlockWithObject registerCompletion = ^(NSString *uuid, NSError *error){
                [waitIndicator hide];
                if(!error) {
                    
                    // store user's access token
                    [UserPreferencesObject writeUserPreference:uuid WithStringValue:USER_REGISTER_UUID];
                    
                    // show message dialog to indicate user to receive verification email
                    [IndicatorObject showDialogWithTitle:@"註冊成功" Message:@"認證郵件已寄給您\n請點選郵件內的連結啟用帳號"];
                    
                    [self dismissViewControllerAnimated:YES completion:^{
                        [self.delegate registerViewControllerDidCompleted];
                    }];
                }
                else {
                }
            };
            
            // AAA register
            [WebService registerWithEmail:self.txtEmail.text
                                 Password:self.txtPassword.text
                                 UserName:self.txtUserName.text
                                    Phone:self.txtPhoneNumber.text
                                   Mobile:self.txtMobileNumber.text
                                  Address:self.txtAddress.text
                               Completion:registerCompletion ];
        }];
    }
}

- (IBAction)pressBtnCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
