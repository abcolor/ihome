//
//  VideoRecordSelectedCell.h
//  iHome
//
//  Created by Liu David on 2014/8/28.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoRecordObject.h"

@protocol VideoRecordCellDelegate <NSObject>
-(void)videoRecordCellDidSelectedMotionEventsWithRecord:(VideoRecordObject*)record;
-(void)videoRecordCellDidSelectedViewVideoRecord:(VideoRecordObject*)record;
@end

@interface VideoRecordSelectedCell : UITableViewCell
@property(nonatomic, assign) id<VideoRecordCellDelegate> delegate;
@property(retain, nonatomic)VideoRecordObject *videoRecord;
@end
