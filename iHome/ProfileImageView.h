//
//  ProfileImageView.h
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileImageView : UIImageView
@property(retain, nonatomic)UIImage *imgProfile;
@end
