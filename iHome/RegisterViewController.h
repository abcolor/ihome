//
//  RegisterViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterViewControllerDelegate <NSObject>
-(void)registerViewControllerDidCompleted;
@end

@interface RegisterViewController : UIViewController
@property(nonatomic, assign) id<RegisterViewControllerDelegate> delegate;
@end
