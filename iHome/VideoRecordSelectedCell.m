//
//  VideoRecordSelectedCell.m
//  iHome
//
//  Created by Liu David on 2014/8/28.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "VideoRecordSelectedCell.h"

@interface VideoRecordSelectedCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgVideoPreview;
@property (weak, nonatomic) IBOutlet UILabel *labVideoDate;
@end


@implementation VideoRecordSelectedCell

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - IBActions

- (IBAction)pressBtnQueryMotionEvents:(id)sender {
    [self.delegate videoRecordCellDidSelectedMotionEventsWithRecord:self.videoRecord];
}

- (IBAction)pressBtnViewVideo:(id)sender {
    [self.delegate videoRecordCellDidSelectedViewVideoRecord:self.videoRecord];
}

#pragma mark - Properties

-(void)setVideoRecord:(VideoRecordObject *)videoRecord {
    
    _videoRecord = videoRecord;
    
    if(videoRecord.imgPreview) self.imgVideoPreview.image = videoRecord.imgPreview;
    self.labVideoDate.text = [NSString stringWithFormat:@"%d/%d/%d %d:%02d", videoRecord.date.year, videoRecord.date.month, videoRecord.date.day, videoRecord.date.hour, videoRecord.date.minute];
}

@end
