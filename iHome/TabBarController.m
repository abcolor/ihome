//
//  TabBarController.m
//  iHome
//
//  Created by Liu David on 2014/8/29.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "TabBarController.h"
#import "EventManagerObject.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // test usage
    [self addTestEvents];
    
    // check unread event number and update event tab badge
    EventManagerObject *eventManager = [EventManagerObject sharedEventManagerObject];
    int unreadEventNumber = 0;
    for(EventObject *event in [eventManager.arrayEvents objectEnumerator]) {
        if(event.unread) unreadEventNumber++;
    }
    if(unreadEventNumber) {
        UITabBarItem *notificationBarItem = [[self.viewControllers objectAtIndex:3] tabBarItem];
        notificationBarItem.badgeValue = [NSString stringWithFormat:@"%d", unreadEventNumber];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)addTestEvents {
    
    EventManagerObject *eventManager = [EventManagerObject sharedEventManagerObject];
    if(eventManager.eventNumber==0) {
        NSArray *eventDescriptions = @[@"偵測移動物體", @"溫度異常警告", @"濕度異常警告"];
        
        for(NSString *description in [eventDescriptions objectEnumerator]) {
            EventObject *event = [[EventObject alloc] init];
            event.eventId = [[NSUUID UUID] UUIDString];
            event.description = description;
            event.unread = YES;
            [eventManager addEvent:event];
        }
    }
}

@end
