//
//  MotionRecordObject.h
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatePickerObject.h"

@interface MotionRecordObject : NSObject
@property(retain)UIImage *imgPreview;
@property(assign)DateComponent date;
@end
