//
//  UserPreferencesObject.m
//  iHome
//
//  Created by Liu David on 2014/8/4.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "UserPreferencesObject.h"

@implementation UserPreferencesObject


#pragma mark - Class Methods

+(void)writeUserPreference:(NSString*)preference WithStringValue:(NSString*)value {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:value forKey:preference];
    [userDefault synchronize];
}

+(void)writeUserPreference:(NSString*)preference WithDictValue:(NSDictionary *)dictValue {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:dictValue];
    [userDefault setObject:encodedObject forKey:preference];
    [userDefault synchronize];
}

+(void)writeUserPreference:(NSString*)preference WithBoolValue:(BOOL)value {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:value forKey:preference];
    [userDefault synchronize];
}

+(NSString*)readStringValueOfUserPreference:(NSString*)preference {
    return [[NSUserDefaults standardUserDefaults] objectForKey:preference];
}

+(NSDictionary*)readDictValueOfUserPreference:(NSString*)preference {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:preference];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+(BOOL)readBoolValueOfUserPreference:(NSString*)preference {
    return [[NSUserDefaults standardUserDefaults] boolForKey:preference];
}

+(void)clearUserPreference:(NSString*)preference {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:nil forKey:preference];
    [userDefault synchronize];
}

@end
