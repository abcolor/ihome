//
//  VideoRecordsViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"

@interface VideoRecordsViewController : UITableViewController
@property(retain)NSArray *arrayVideoRecords;
@property(retain)DeviceObject *device;
@end
