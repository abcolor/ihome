//
//  DeviceObject.h
//  iHome
//
//  Created by Liu David on 2014/8/8.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceObject : NSObject
@property(retain)NSString *deviceName;
@property(retain)NSString *deviceId;
@property(retain)NSString *macAddress;
@property(assign)BOOL videoStreamEnabled;
@property(assign)BOOL audioStreamEnabled;
//@property(retain)UIImage *devicePhoto;
@end
