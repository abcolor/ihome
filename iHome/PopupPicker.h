//
//  PopupPicker.h
//  CommandP
//
//  Created by exe on 2014/9/20.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PopupPicker;

// When the picker sheet is dismissed, this block is called
typedef void(^PickerCompletionBlock)(PopupPicker *picker, BOOL cancelled, NSArray *selectedComponentIndexes);

// Individuall callback when a rotating wheel value changes
typedef void(^PickerChangedBlock)(PopupPicker *picker, NSArray *componentValues, int selectedItem);

@interface PopupPicker : NSObject
@property (strong, nonatomic) NSString *textCancel;
@property (strong, nonatomic) NSString *textDone;

@property (strong, nonatomic) NSString *selectedItem;

-(id)initWithCompletion:(PickerCompletionBlock)completionBlock;

-(void)addComponent:(NSArray*)rowValues
   initialSelection:(int)initialSelection
            changed:(PickerChangedBlock)changedBlock;

-(void)show;
-(void)hide;

@end
