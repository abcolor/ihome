//
//  VideoCell.m
//  iHome
//
//  Created by Liu David on 2014/8/26.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties

-(void)setDevice:(DeviceObject *)device {
    _device = device;
    
    self.labDeviceName.text = device.deviceName;
    
    if(device.videoStreamEnabled)
        self.imgVideoStatus.image = [UIImage imageNamed:@"video_enabled"];
    else
        self.imgVideoStatus.image = [UIImage imageNamed:@"video_disabled"];

    if(device.audioStreamEnabled)
        self.imgAudioStatus.image = [UIImage imageNamed:@"audio_enabled"];
    else
        self.imgAudioStatus.image = [UIImage imageNamed:@"audio_disabled"];

    
}

@end
