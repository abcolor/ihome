//
//  FullscreenVideoViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "FullscreenVideoViewController.h"

@interface FullscreenVideoViewController ()

@end

@implementation FullscreenVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // TODO: set device's video to self.view
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)pressBtnClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
