//
//  QueryVideoRecordViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "QueryVideoRecordViewController.h"
#import "DatePickerObject.h"
#import "IndicatorObject.h"
#import "VideoRecordObject.h"
#import "VideoRecordsViewController.h"
#import "QueryVideoNavigationController.h"

@interface QueryVideoRecordViewController () {
    DateComponent beginDate;
    DateComponent endDate;
    DeviceObject *device;
}

@property (weak, nonatomic) IBOutlet UILabel *labDeviceName;
@property (weak, nonatomic) IBOutlet UIButton *btnBeginDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;

@end

@implementation QueryVideoRecordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    QueryVideoNavigationController *nav = (QueryVideoNavigationController*)self.navigationController;
    device = nav.device;

    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:date];
    int currentYear = [components year];
    int currentMonth = [components month];
    int currentDay = [components day];
    int currentHour = [components hour];
    int currentMinute = [components minute];
    DateComponent currentDate = DateComponentMake(currentYear, currentMonth, currentDay, currentHour, currentMinute);
    beginDate = currentDate;
    endDate = currentDate;
    
    [self setupUserInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"關閉" style:UIBarButtonItemStylePlain target:self action:@selector(pressBtnCancel:)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
    // device name
    self.labDeviceName.text = device.deviceName;
    
    // query date
    [self.btnBeginDate setTitle:[NSString stringWithFormat:@"%d/%d/%d %d:%02d", beginDate.year, beginDate.month, beginDate.day, beginDate.hour, beginDate.minute] forState:UIControlStateNormal];
    [self.btnEndDate setTitle:[NSString stringWithFormat:@"%d/%d/%d %d:%02d", endDate.year, endDate.month, endDate.day, endDate.hour, endDate.minute] forState:UIControlStateNormal];
}

#pragma mark - IBActions

-(void)pressBtnCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressBtnSelectBeginDate:(id)sender {
    [DatePickerObject showWithView:self.view Type:kDatePickerTypeDateAndTime InitialDate:beginDate Completion:^(BOOL cancel, DateComponent date){
        if(!cancel) {
            beginDate = date;
            [self.btnBeginDate setTitle:[NSString stringWithFormat:@"%d/%d/%d %d:%02d", beginDate.year, beginDate.month, beginDate.day, beginDate.hour, beginDate.minute] forState:UIControlStateNormal];
        }
    }];
}

- (IBAction)pressBtnSelectEndDate:(id)sender {
    [DatePickerObject showWithView:self.view Type:kDatePickerTypeDateAndTime InitialDate:endDate Completion:^(BOOL cancel, DateComponent date){
        if(!cancel) {
            endDate = date;
            [self.btnEndDate setTitle:[NSString stringWithFormat:@"%d/%d/%d %d:%02d", endDate.year, endDate.month, endDate.day, endDate.hour, endDate.minute] forState:UIControlStateNormal];
        }
    }];
}

- (IBAction)pressBtnQuery:(id)sender {
    
    // TODO: integration
    // 1. request video records with deviceId, beginDate and endDate
    
    // 2. video recordsreceived,
    VideoRecordsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"video_records"];
    vc.device = device;
    
    // test usage
    NSMutableArray *arrayVideoRecords = [NSMutableArray array];
    for(int i=0; i<5; i++) {
        VideoRecordObject *videoRecord = [[VideoRecordObject alloc] init];
        videoRecord.date = DateComponentMake(2014, 9, 13, 15, 30);
        videoRecord.imgPreview = nil;
        [arrayVideoRecords addObject:videoRecord];
    }
    vc.arrayVideoRecords = [NSArray arrayWithArray:arrayVideoRecords];
        
    [self.navigationController pushViewController:vc animated:YES];
}

@end
