//
//  VideoDetailViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "VideoDetailViewController.h"
#import "DeviceManagerObject.h"
#import "QueryVideoNavigationController.h"
#import "FullscreenVideoViewController.h"

@interface VideoDetailViewController () <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *switchVideoStreamEnabled;
@property (weak, nonatomic) IBOutlet UISwitch *switchAudioStreamEnabled;
@property (weak, nonatomic) IBOutlet UIView *viewVideo;
@end

@implementation VideoDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUserInterface];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGestureRecognizer.delegate = self;
    [self.viewVideo addGestureRecognizer:tapGestureRecognizer];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(self.device)
        [[DeviceManagerObject sharedDeviceManagerObject] updateDevice:self.device];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // navigation bar title
    self.navigationItem.title = self.device.deviceName;

    // device info
    self.switchVideoStreamEnabled.on = self.device.videoStreamEnabled;
    self.switchAudioStreamEnabled.on = self.device.audioStreamEnabled;
    
    UIColor *color = [UIColor colorWithRed:0.96f green:0.05 blue:0.21 alpha:1];
    self.switchVideoStreamEnabled.onTintColor = color;
    self.switchAudioStreamEnabled.onTintColor = color;
}

#pragma mark - Gesture Handlers

- (void)handleTap:(UIPanGestureRecognizer *)gestureRecognizer {
    FullscreenVideoViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"fullscreen_video"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.device = self.device;
    [self presentViewController:vc animated:YES completion:nil];}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeVideoStreamEnableValue:(id)sender {
    self.device.videoStreamEnabled = self.switchVideoStreamEnabled.on;
}

- (IBAction)changeAudioStreamEnableValue:(id)sender {
    self.device.audioStreamEnabled = self.switchAudioStreamEnabled.on;
}

- (IBAction)pressBtnQuery:(id)sender {
    QueryVideoNavigationController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"query_video_record"];
    vc.device = self.device;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
