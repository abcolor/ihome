//
//  SensorViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/20.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "SensorViewController.h"
#import "DeviceManagerObject.h"
#import "SensorCell.h"
#import "SensorInfoViewController.h"

@interface SensorViewController () {
    DeviceManagerObject *deviceManager;
}
@end

@implementation SensorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    deviceManager = [DeviceManagerObject sharedDeviceManagerObject];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [deviceManager deviceNumber];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"sensor_cell";
    SensorCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.device = [deviceManager.arrayDevices objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SensorInfoViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"sensor_info"];
    vc.device = [deviceManager.arrayDevices objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
