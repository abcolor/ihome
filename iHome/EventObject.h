//
//  EventObject.h
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventObject : NSObject
@property(retain)NSString *eventId;
@property(retain)NSString *description;
@property(assign)BOOL unread;
@end
