//
//  VideoCell.h
//  iHome
//
//  Created by Liu David on 2014/8/26.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"

@interface VideoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labDeviceName;
@property (weak, nonatomic) IBOutlet UIImageView *imgDevice;
@property (weak, nonatomic) IBOutlet UIImageView *imgVideoStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgAudioStatus;
@property(retain, nonatomic)DeviceObject *device;

@end
