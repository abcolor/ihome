//
//  SignInViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/4.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignInViewControllerDelegate <NSObject>
-(void)signInViewControllerDidSelectRegister;
-(void)signInViewControllerDidCompleted;
@end

@interface SignInViewController : UIViewController
@property(nonatomic, assign) id<SignInViewControllerDelegate> delegate;
@end
