//
//  VideoRecordsViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "VideoRecordsViewController.h"
#import "VideoRecordCell.h"
#import "VideoRecordSelectedCell.h"
#import "MotionRecordObject.h"
#import "MotionRecordsViewController.h"

@interface VideoRecordsViewController ()<VideoRecordCellDelegate> {
    int selectedRow;
    UITableViewCell *prevSelectedCell;
}
@end

@implementation VideoRecordsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupUserInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // title
    self.navigationItem.title = self.device.deviceName;
    
    prevSelectedCell = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.arrayVideoRecords count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row==selectedRow)
        return 101;
    else
        return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==selectedRow) {
        static NSString *SelectedCellIdentifier = @"video_record_selected_cell";
        VideoRecordSelectedCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectedCellIdentifier forIndexPath:indexPath];
        
        cell.videoRecord = [self.arrayVideoRecords objectAtIndex:indexPath.row];
        cell.delegate = self;
        
        prevSelectedCell.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1];
        prevSelectedCell = cell;
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"video_record_cell";
        VideoRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.videoRecord = [self.arrayVideoRecords objectAtIndex:indexPath.row];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(selectedRow!=indexPath.row) {
        selectedRow = indexPath.row;
        
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        NSIndexPath *prevIndexPath = [tableView indexPathForCell:prevSelectedCell];
        
        NSMutableArray *array = [NSMutableArray array];
        [array addObject:reloadIndexPath];
        if(prevIndexPath) [array addObject:prevIndexPath];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithArray:array]
                         withRowAnimation:UITableViewRowAnimationNone];
    }
    else {
        [tableView reloadData];
    }
}

#pragma mark - VideoRecordCellDelegate

-(void)videoRecordCellDidSelectedViewVideoRecord:(VideoRecordObject *)record {
}

-(void)videoRecordCellDidSelectedMotionEventsWithRecord:(VideoRecordObject *)record {
    
    // TODO: integration
    // 1. request motion event records with video record, beginDate and endDate
    
    
    // 2. motion event records received
    MotionRecordsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"motion_records"];
    
    // test usage
    NSMutableArray *arrayMotionRecords = [NSMutableArray array];
    for(int i=0; i<10; i++) {
        MotionRecordObject *motionRecord = [[MotionRecordObject alloc] init];
        motionRecord.date = DateComponentMake(2014, 4, 22, 15, 30);
        motionRecord.imgPreview = nil;
        [arrayMotionRecords addObject:motionRecord];
    }
    vc.arrayMotionRecords = [NSArray arrayWithArray:arrayMotionRecords];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
