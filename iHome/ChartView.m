//
//  ChartView.m
//  iHome
//
//  Created by Liu David on 2014/8/20.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "ChartView.h"

@interface ChartView() <UIGestureRecognizerDelegate> {
    UIPanGestureRecognizer *panGesture;
    UIGestureRecognizerState touchState;
    CGPoint touchPoint;
}
@end

@implementation ChartView

@synthesize viewDetailsEnabled = _viewDetailsEnabled;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if(!self.arrayChartValues) return;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // plot signal data
    CGContextSetLineWidth(context, 1.0);
    
    CGRect rectBoundary = CGRectMake(rect.origin.x + CHART_VIEW_BOUNDARY,
                                     rect.origin.y + CHART_VIEW_BOUNDARY,
                                     rect.size.width - (CHART_VIEW_BOUNDARY * 2),
                                     rect.size.height - (CHART_VIEW_BOUNDARY * 2));
    
    
    CGContextAddRect(context, rectBoundary);
    CGContextSetRGBStrokeColor(context, 0.8, 0.8, 0.8, 1.0);
    CGContextStrokePath(context);
    
    // insert null value to arrayChartValues at begin & end for single input data
    if([self.arrayChartValues count]==1) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.arrayChartValues];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:@"" forKey:@"label"];        
        [array insertObject:dict atIndex:0];
        
        dict = [NSMutableDictionary dictionary];
        [dict setObject:@"" forKey:@"label"];
        [array addObject:dict];
        self.arrayChartValues = [NSArray arrayWithArray:array];
        
        self.xAxisLabelSkips = 1;
    }
        
    int pointNumber = (int)[self.arrayChartValues count];

    CGFloat chartWidth = rect.size.width - (CHART_VIEW_BOUNDARY * 2);
    CGFloat chartHeight = rect.size.height - (CHART_VIEW_BOUNDARY * 2);
    
    
    // draw labels on X and Y axis
    NSStringDrawingContext *drawingContext = [[NSStringDrawingContext alloc] init];
    CGFloat xOffset = chartWidth / (CGFloat)(pointNumber - 1);
    CGFloat yOffset = chartHeight / (CGFloat)(self.valueLevels - 1);
    
    CGFloat textWidth = 30;
    CGFloat textHeight = 15;
    

    // draw value labels on Y axis
    int dataValue = self.minValue;
    int dataSkip = (self.maxValue - self.minValue) / (self.valueLevels - 1);
    for(int i=0; i<self.valueLevels; i++) {
        NSMutableParagraphStyle *paragrapStyle = NSMutableParagraphStyle.new;
        paragrapStyle.alignment = NSTextAlignmentRight;
        NSDictionary *textAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Light" size:10],
                                         NSParagraphStyleAttributeName:paragrapStyle};
        
        NSString *string = [NSString stringWithFormat:@"%d", dataValue];
 
        CGFloat textOffsetX = CHART_VIEW_BOUNDARY - textWidth - 4;
        CGFloat textOffsetY = (rect.size.height - CHART_VIEW_BOUNDARY) - (i * yOffset) - (textHeight / 2.0f) + 2;
        
        CGRect drawRect = CGRectMake(textOffsetX,
                                     textOffsetY,
                                     textWidth,
                                     textHeight);
        
        [string drawWithRect:drawRect
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:textAttributes
                     context:drawingContext];
        
        dataValue += dataSkip;
    }
    
    // draw labels on X axis
    for(int i=0; i<pointNumber; i+=self.xAxisLabelSkips) {
        NSMutableParagraphStyle *paragrapStyle = NSMutableParagraphStyle.new;
        paragrapStyle.alignment = NSTextAlignmentCenter;
        
        NSDictionary *textAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Light" size:10],
                                         NSParagraphStyleAttributeName:paragrapStyle};
        
        NSDictionary *dict = [self.arrayChartValues objectAtIndex:i];
        NSString *string = [dict objectForKey:@"label"];
        CGFloat textOffsetX = ((i * xOffset) + CHART_VIEW_BOUNDARY) - (textWidth / 2.0f);
        CGFloat textOffsetY = rect.size.height - CHART_VIEW_BOUNDARY + 6;
        
        CGRect drawRect = CGRectMake(textOffsetX,
                                     textOffsetY,
                                     textWidth,
                                     textHeight);
        
        [string drawWithRect:drawRect
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:textAttributes
                     context:drawingContext];
    }
    
    // plot arrayChartValues
    CGFloat prevX, prevY;
    BOOL skipPlot;
    
    for(int i=0; i<pointNumber; i++) {
        NSDictionary *dict = [self.arrayChartValues objectAtIndex:i];
        NSNumber *value = [dict objectForKey:@"value"];
        BOOL drawSamplePoint = YES;
        BOOL drawLine = YES;
        
        if((i==0)||(i==(pointNumber-1))) {
            if(!value) {
                value = [NSNumber numberWithInt:0];
                drawSamplePoint = NO;
                drawLine = NO;
                if(i==0) skipPlot = YES;
            }
        }
        else if((value)&&(skipPlot)) {
            skipPlot = NO;
            drawLine = NO;
        }
        
        if(value) {
            CGFloat x = (i * xOffset) + CHART_VIEW_BOUNDARY;
            CGFloat y = (([value intValue] - self.minValue) * chartHeight) / (self.maxValue - self.minValue);
            y = (chartHeight - y) + CHART_VIEW_BOUNDARY;
            
            if(drawSamplePoint) {
                CGContextAddEllipseInRect(context, CGRectMake(x-1, y-1, 2, 2));
            }
            
            if((i)&&(!self.chartLineHidden)&&(drawLine)) {
                CGContextMoveToPoint(context, prevX, prevY);
                CGContextAddLineToPoint(context, x, y);
            }

            prevX = x;
            prevY = y;
        }
    }
    CGContextSetRGBStrokeColor(context, 1, 0.16, 0.255, 1.0);
    CGContextStrokePath(context);
    
    // draw touch line
    switch (touchState) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged: {
            
            for(int i=0; i<pointNumber; i++) {
                NSDictionary *dict = [self.arrayChartValues objectAtIndex:i];
                NSString *label = [dict objectForKey:@"label"];
                NSNumber *value = [dict objectForKey:@"value"];
                
                CGFloat x = (i * xOffset) + CHART_VIEW_BOUNDARY;
                CGFloat y = (([value intValue] - self.minValue) * chartHeight) / (self.maxValue - self.minValue);
                y = (chartHeight - y) + CHART_VIEW_BOUNDARY;
                
                if((x > touchPoint.x)&&(value)&&( fabs(x - touchPoint.x) <= xOffset )) {
                    
                    CGContextSetRGBStrokeColor(context, 0.8, 0.8, 0.8, 1.0);

                    // highlight data point
                    CGContextAddEllipseInRect(context, CGRectMake(x-2, y-2, 4, 4));
                    
                    // draw vertical line
                    CGContextMoveToPoint(context, x, CHART_VIEW_BOUNDARY);
                    CGContextAddLineToPoint(context, x, CHART_VIEW_BOUNDARY + chartHeight);
                    
                    // draw label and data value on top
                    NSMutableParagraphStyle *paragrapStyle = NSMutableParagraphStyle.new;
                    paragrapStyle.alignment = NSTextAlignmentCenter;
                    
                    NSDictionary *textAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Light" size:10],
                                                     NSParagraphStyleAttributeName:paragrapStyle};
                    
                    
                    NSString *string = [NSString stringWithFormat:@"%@  %d%@", label, [value intValue], self.dataUnit];
                    
                    textWidth = 150;
                    textHeight = 15;
                    CGRect drawRect = CGRectMake(x - (textWidth / 2.0f),
                                                 4,
                                                 textWidth,
                                                 textHeight);
                    
                    [string drawWithRect:drawRect
                                 options:NSStringDrawingUsesLineFragmentOrigin
                              attributes:textAttributes
                                 context:drawingContext];
                    
                    CGContextSetRGBStrokeColor(context, 1, 0.16, 0.255, 0.5);
                    CGContextStrokePath(context);
                    
                    break;
                }
            }
        }   break;
                        
        default:
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
        } break;
    }

    

}


#pragma mark - Gesture Handlers

- (void)handlePan:(UIPanGestureRecognizer *)gestureRecognizer {
    
    touchState = [gestureRecognizer state];
	touchPoint = [gestureRecognizer locationInView:self];
    [self setNeedsDisplay];
}

#pragma mark - Properties

-(void)setViewDetailsEnabled:(BOOL)viewDetailsEnabled {

    _viewDetailsEnabled = viewDetailsEnabled;
    
    if(viewDetailsEnabled) {
        if(!panGesture) {
            panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
            panGesture.delegate = self;
            [self addGestureRecognizer:panGesture];
        }
    }
    else {
        if(panGesture) {
            [self removeGestureRecognizer:panGesture];
            panGesture.delegate = nil;
            panGesture = nil;
        }
    }
}

@end
