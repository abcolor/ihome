//
//  FileManagementHelper.h
//  CommandP
//
//  Created by Liu David on 2014/2/12.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManagementHelper : NSObject
+(NSString*)getDocumentDirectoryPath;
+(NSString*)getPathWithName:(NSString*)name;
+(NSString*)getPathWithName:(NSString*)name atDirectory:(NSString*)directory;
+(NSString*)getTempPathWithName:(NSString*)name;
+(void)removeFileAtPath:(NSString*)path;
+(BOOL)createDirectory:(NSString*)name;

@end
