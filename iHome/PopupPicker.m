//
//  PopupPicker.m
//  CommandP
//
//  Created by exe on 2014/9/20.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "PopupPicker.h"

@interface PopupPicker () <UIPickerViewDataSource, UIPickerViewDelegate>
{
    UIView *backgroundMask_;
    UIView *actionBar_;
    UIButton *btnCancel_;
    UIButton * btnDone_;
    UIPickerView *pickerView_;
    
    PickerCompletionBlock completionBlock_;
    PickerChangedBlock changedBlock_;
    
    NSMutableArray* componentsData_;
    NSMutableArray* componentsInitSelect_;
}
@property (nonatomic, strong) NSMutableArray *componentChangeBlocks;
@end

@implementation PopupPicker

-(id)initWithCompletion:(PickerCompletionBlock)completionBlock
{
    self = [super init];
    if (self)
    {
        completionBlock_ = completionBlock;
        
        backgroundMask_ = [[UIView alloc] initWithFrame:CGRectZero];
        backgroundMask_.opaque = NO;
        
        actionBar_ = [[UIView alloc] initWithFrame:CGRectZero];
        actionBar_.backgroundColor = [UIColor colorWithRed:126/255.f green:138/255.f blue:153/255.f alpha:1.f];
        [backgroundMask_ addSubview:actionBar_];
        
        UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        
        btnCancel_ = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCancel_ setTitle:@"取消" forState:UIControlStateNormal];
        [btnCancel_.titleLabel setFont:font];
        [btnCancel_ sizeToFit];
        [btnCancel_ addTarget:self action:@selector(onCancel) forControlEvents:UIControlEventTouchUpInside];
        [actionBar_ addSubview:btnCancel_];
        
        btnDone_ = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone_ setTitle:@"確認" forState:UIControlStateNormal];
        [btnDone_.titleLabel setFont:font];
        [btnDone_ sizeToFit];
        [btnDone_ addTarget:self action:@selector(onDone) forControlEvents:UIControlEventTouchUpInside];
        [actionBar_ addSubview:btnDone_];
        
        pickerView_ = [[UIPickerView alloc] initWithFrame:CGRectZero];
        pickerView_.backgroundColor = [UIColor whiteColor];
        pickerView_.dataSource = self;
        pickerView_.delegate = self;
        [backgroundMask_ addSubview:pickerView_];
        
        componentsData_ = [NSMutableArray array];
        componentsInitSelect_ = [NSMutableArray array];
        _componentChangeBlocks = [NSMutableArray array];
    }
    return self;
}

-(id)init
{
    return [self initWithCompletion:nil];
}

-(void)addComponent:(NSArray*)rowValues
   initialSelection:(int)initialSelection
            changed:(PickerChangedBlock)changedBlock
{
    [componentsData_ addObject:rowValues];
    [componentsInitSelect_ addObject:[NSNumber numberWithInt:initialSelection]];

    [pickerView_ reloadAllComponents];
    [pickerView_ selectRow:initialSelection inComponent:componentsData_.count - 1 animated:NO];

    changedBlock_ = changedBlock;
    
    if(changedBlock) {
        PickerChangedBlock blockCopy = [changedBlock copy];
        [_componentChangeBlocks addObject:blockCopy];
    }
    else
        [_componentChangeBlocks addObject:[NSNull null]];
}

-(void)show
{
    CGSize baseSize = [UIScreen mainScreen].bounds.size;
    
    CGRect frame;
    
    backgroundMask_.frame = CGRectMake(0, 0, baseSize.width, baseSize.height);
    
    //
    
    frame = pickerView_.frame;
    frame.origin.y = baseSize.height - frame.size.height;
    CGRect pickerEndFrame = frame;
    
    frame.origin.y = baseSize.height + 44;
    pickerView_.frame = frame;
    
    //
    
    frame = CGRectMake(0, pickerEndFrame.origin.y - 44, baseSize.width, 44);
    CGRect barEndFrame = frame;
    
    frame.origin.y = baseSize.height;
    actionBar_.frame = frame;
    
    //
    
    frame = btnCancel_.frame;
    frame.origin.x = 15;
    frame.origin.y = (barEndFrame.size.height - frame.size.height) * 0.5f;
    btnCancel_.frame = frame;
    
    //
    
    frame = btnDone_.frame;
    frame.origin.x = barEndFrame.size.width - 15 - frame.size.width;
    frame.origin.y = (barEndFrame.size.height - frame.size.height) * 0.5f;
    btnDone_.frame = frame;
    
    //
    
    [[self getTopNonModalWindow] addSubview:backgroundMask_];
    
    //
    
    backgroundMask_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.f];
    
    [UIView animateWithDuration:0.25f animations:^{
        
        backgroundMask_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.5f];
        pickerView_.frame = pickerEndFrame;
        actionBar_.frame = barEndFrame;
        
    }];
}

- (void)hide
{
    CGRect barEndFrame = actionBar_.frame;
    barEndFrame.origin.y = backgroundMask_.frame.size.height;
    CGRect pickerEndFrame = pickerView_.frame;
    pickerEndFrame.origin.y = backgroundMask_.frame.size.height + barEndFrame.size.height;
    
    [UIView animateWithDuration:0.25f animations:^{
        
        backgroundMask_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.f];
        pickerView_.frame = pickerEndFrame;
        actionBar_.frame = barEndFrame;
        
    } completion:^(BOOL finished) {
         [backgroundMask_ removeFromSuperview];
    }];
}

#pragma mark -

- (UIWindow*)getTopNonModalWindow
{
    UIWindow* topNonModalWindow = nil;
    for (UIWindow* win in [[UIApplication sharedApplication].windows reverseObjectEnumerator])
    {
        if ([win class] == [UIWindow class])
        {
            topNonModalWindow = win;
            break;
        }
    }
    
    NSAssert(topNonModalWindow, @"");
    
    return topNonModalWindow;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return componentsData_.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSArray* rows = [componentsData_ objectAtIndex:component];
    return rows.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSArray* rows = [componentsData_ objectAtIndex:component];
    return [rows objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSArray* rows = [componentsData_ objectAtIndex:component];
    
    id changedActionBlock = [_componentChangeBlocks objectAtIndex:component];
    if(changedActionBlock && ( changedActionBlock != [NSNull null]) )
    {
        PickerChangedBlock changedBlock = (PickerChangedBlock) changedActionBlock;
        changedBlock(self, rows, (int)row);
    }
    
    //if (changedBlock_)
      //  changedBlock_(self, rows, (int)row);
}

#pragma mark - UIButton action

- (void)onCancel
{
    if (completionBlock_)
        completionBlock_(self, YES, nil);
    
    [self hide];
}

- (void)onDone
{
    if (completionBlock_)
    {
        NSMutableArray *selectedIndexs = [NSMutableArray array];
        for (int i = 0; i < componentsData_.count; ++i)
        {
            NSInteger selectedRow = [pickerView_ selectedRowInComponent:i];
            [selectedIndexs addObject:[NSNumber numberWithInteger:selectedRow]];
        }
        
        completionBlock_(self, NO, selectedIndexs);
    }
    
    [self hide];
}

#pragma mark - properties

- (NSString*)textCancel
{
    return [btnCancel_ titleForState:UIControlStateNormal];
}

- (void)setTextCancel:(NSString *)textCancel
{
    [btnCancel_ setTitle:textCancel forState:UIControlStateNormal];
    [btnCancel_ sizeToFit];
}

- (NSString*)textDone
{
    return [btnDone_ titleForState:UIControlStateNormal];
}

- (void)setTextDone:(NSString *)textDone
{
    [btnDone_ setTitle:textDone forState:UIControlStateNormal];
    [btnDone_ sizeToFit];
}

- (NSString*)selectedItem
{
    NSArray *rows = [componentsData_ objectAtIndex:0];
    return [rows objectAtIndex:[pickerView_ selectedRowInComponent:0]];
}

- (void)setSelectedItem:(NSString *)selectedItem
{
    NSArray *rows = [componentsData_ objectAtIndex:0];
    NSInteger index = [rows indexOfObject:selectedItem];
    [pickerView_ selectRow:index inComponent:0 animated:NO];
}

@end
