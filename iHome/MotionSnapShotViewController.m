//
//  MotionSnapShotViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "MotionSnapShotViewController.h"

@interface MotionSnapShotViewController ()

@end

@implementation MotionSnapShotViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUserInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // title
    self.navigationItem.title = [NSString stringWithFormat:@"%d/%d/%d %d:%02d", self.motionRecord.date.year, self.motionRecord.date.month, self.motionRecord.date.day, self.motionRecord.date.hour, self.motionRecord.date.minute];
}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
