//
//  MotionRecordCell.h
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MotionRecordObject.h"

@interface MotionRecordCell : UITableViewCell
@property(retain, nonatomic)MotionRecordObject *motionRecord;
@end
