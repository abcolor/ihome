//
//  IndicatorObject.h
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IndicatorObject : NSObject
+(void)showDialogWithTitle:(NSString*)title Message:(NSString*)msg;
-(id)initWithView:(UIView*)view;
-(void)show;
-(void)hide;
@end
