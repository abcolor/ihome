//
//  EventCell.h
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventObject.h"

@interface EventCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labEventDescription;

@property(retain, nonatomic)EventObject *event;
@end
