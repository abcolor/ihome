//
//  SensorCell.h
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"

@interface SensorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labDeviceName;
@property (weak, nonatomic) IBOutlet UILabel *labTemperature;
@property (weak, nonatomic) IBOutlet UILabel *labHumidity;
@property(retain, nonatomic)DeviceObject *device;
@end
