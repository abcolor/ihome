//
//  DeviceViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "DeviceViewController.h"
#import "DeviceInfoViewController.h"
#import "ScanDeviceViewController.h"
#import "DeviceManagerObject.h"
#import "DeviceCell.h"

@interface DeviceViewController ()<ScanDeviceViewControllerDelegate> {
    DeviceManagerObject *deviceManager;
}
@end

@implementation DeviceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Right navigation button: add device
    UIImage *img = [UIImage imageNamed:@"add"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressAddDeviceBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    deviceManager = [DeviceManagerObject sharedDeviceManagerObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [deviceManager deviceNumber];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"device_cell";
    DeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.device = [deviceManager.arrayDevices objectAtIndex:indexPath.row];    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {     
    DeviceInfoViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"device_info"];
    vc.device = [deviceManager.arrayDevices objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - ScanDeviceViewControllerDelegate

-(void)scanDeviceViewControllerDidFoundInfo:(NSString *)foundInfo {
    NSLog(@"scanned info: %@", foundInfo);
    
    // TODO: register new device
    
    // new device info, testing usage...
    DeviceObject *device = [[DeviceObject alloc] init];
    device.deviceId = [[NSUUID UUID] UUIDString];
    device.deviceName = @"裝置xyz";
    device.macAddress = @"1234";
    device.videoStreamEnabled = NO;
    device.audioStreamEnabled = NO;
    
    [deviceManager addDevice:device];
    [self.tableView reloadData];
}

#pragma mark - Button handlers & IBActions

-(void)pressAddDeviceBtn:(id)sender {
    ScanDeviceViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"add_device"];
    vc.delegate = self;
    vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:vc animated:YES completion:nil];
}


@end
