//
//  IndicatorObject.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "IndicatorObject.h"

@interface IndicatorObject() {
    UIImageView *imgLoading;
    UIView *viewBackground;
}
@end

@implementation IndicatorObject

#pragma mark - Class Method

+(void)showDialogWithTitle:(NSString*)title Message:(NSString*)msg {
    UIAlertView *prompt = [UIAlertView alloc];
    prompt = [prompt initWithTitle:title
                           message:msg
                          delegate:nil
                 cancelButtonTitle:@"Ok"
                 otherButtonTitles: nil];
    [prompt show];
}

#pragma mark - Instance Methods

-(id)initWithView:(UIView*)view {
    if(self = [super init]) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        viewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
        viewBackground.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
        viewBackground.alpha = 0;
        
        imgLoading = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading"]];
        imgLoading.center = CGPointMake(screenSize.width / 2.0f, screenSize.height / 2.0f);
        imgLoading.alpha = 0;
        [viewBackground addSubview:imgLoading];
        [viewBackground addSubview:imgLoading];
        
        [view addSubview:viewBackground];
    }
    return self;
}


-(void)show {
    [self runSpinAnimationOnView:imgLoading duration:1.0f rotations:0.8f repeat:HUGE_VALF];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         imgLoading.alpha = 1;
                         viewBackground.alpha = 1;
                     }];
}

-(void)hide {
    imgLoading.hidden = YES;
    viewBackground.hidden = YES;
    
    
    [imgLoading.layer removeAllAnimations];
    [imgLoading removeFromSuperview];
    [viewBackground removeFromSuperview];
}

#pragma mark -

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
