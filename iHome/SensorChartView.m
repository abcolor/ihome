//
//  SensorChartView.m
//  iHome
//
//  Created by Liu David on 2014/8/25.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "SensorChartView.h"

@interface SensorChartView() {
    SensorDataType sensorDataType;
}
@end

@implementation SensorChartView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(id)initWithFrame:(CGRect)frame DataType:(SensorDataType)dataType {
    if(self = [super initWithFrame:frame]) {
        
        sensorDataType = dataType;        
        self.backgroundColor = [UIColor clearColor];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(-18, 69, 22, 21)];
        title.backgroundColor = [UIColor clearColor];
        title.shadowColor = [UIColor clearColor];
        title.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
        title.font = [UIFont fontWithName:@"Helvetica-Light" size:10];
        title.textAlignment = NSTextAlignmentCenter;
        if(sensorDataType==kSensorTemperatureDataType) {
            title.text = @"(°C)";
            self.dataUnit = @"°";
        }
        else {
            title.text = @"(%)";
            self.dataUnit = @"%";
        }
        [self addSubview:title];
    }
    return self;
}


@end
