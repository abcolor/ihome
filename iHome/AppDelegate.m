//
//  AppDelegate.m
//  iHome
//
//  Created by Liu David on 2014/8/1.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "AppDelegate.h"
#import "UserPreferencesObject.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
  
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *vc;
    NSDictionary *dictUserInfo = [UserPreferencesObject readDictValueOfUserPreference:USER_SIGNIN_INFO];
    BOOL autoSignIn = [UserPreferencesObject readBoolValueOfUserPreference:PREFERENCES_AUTO_SIGNIN];
    
    if((dictUserInfo)&&(autoSignIn))
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"main_functions"];
    else
        vc = [[UIStoryboard storyboardWithName:@"Signin" bundle:nil] instantiateViewControllerWithIdentifier:@"sign_in"];
    
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.988 green:0.25 blue:0.298 alpha:1]];

    // setup system failure trace
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

@end
