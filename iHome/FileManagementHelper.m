//
//  FileManagementHelper.m
//  CommandP
//
//  Created by Liu David on 2014/2/12.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "FileManagementHelper.h"

@implementation FileManagementHelper

+(NSString*)getDocumentDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+(NSString*)getPathWithName:(NSString*)name atDirectory:(NSString*)directory {
    if(directory)
        return [[[FileManagementHelper getDocumentDirectoryPath] stringByAppendingPathComponent:directory] stringByAppendingPathComponent:name];
    else
        return [[FileManagementHelper getDocumentDirectoryPath] stringByAppendingPathComponent:name];
}

+(NSString*)getPathWithName:(NSString*)name {
    return [FileManagementHelper getPathWithName:name atDirectory:nil];
}


+(NSString*)getTempPathWithName:(NSString*)name {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:name];
}

+(void)removeFileAtPath:(NSString*)path {
    NSError *error;
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:path]) {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (!success) {
            NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }
    }
}

+(BOOL)createDirectory:(NSString*)name {
	NSString *dirPath = [self getPathWithName:name atDirectory:nil];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:dirPath]==NO) {
		NSLog(@"create new dir: %@", dirPath);
		if([fileManager createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:nil]==NO) {
			NSLog(@"fail to create directory:%@", name);
			return NO;
		}
	}
	return YES;
}

@end
