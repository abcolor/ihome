//
//  SensorInfoViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "SensorInfoViewController.h"
#import "QuerySensorHistoryViewController.h"
#import "SensorChartView.h"

@interface SensorInfoViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labCurrentTemperature;
@property (weak, nonatomic) IBOutlet UILabel *labMaxTemperature;
@property (weak, nonatomic) IBOutlet UILabel *labMinTemperature;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewTemperture;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControlTemperture;

@property (weak, nonatomic) IBOutlet UILabel *labCurrentHumidity;
@property (weak, nonatomic) IBOutlet UILabel *labMaxHumidity;
@property (weak, nonatomic) IBOutlet UILabel *labMinHumidity;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewHumidity;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControlHumidity;
@end

@implementation SensorInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupUserInterface];
    [self requestTemperatureData];
    [self requestHumidityData];   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // navigation bar title
    self.navigationItem.title = self.device.deviceName;
    
    // setup scroll view
    self.scrollViewTemperture.contentSize = CGSizeMake(self.scrollViewTemperture.frame.size.width * 2, self.scrollViewTemperture.frame.size.height);
    self.scrollViewTemperture.pagingEnabled = YES;
    self.scrollViewTemperture.delegate = self;
    self.scrollViewTemperture.tag = 1;
    
    self.scrollViewHumidity.contentSize = CGSizeMake(self.scrollViewTemperture.frame.size.width * 2, self.scrollViewTemperture.frame.size.height);
    self.scrollViewHumidity.pagingEnabled = YES;
    self.scrollViewHumidity.delegate = self;
    self.scrollViewHumidity.tag = 2;
}

-(void)requestTemperatureData {
    
    // TODO: request current temperature data from server
    int currentTemperature = 25;
    int minTemperature = 18;
    int maxTemperature = 36;
    
    self.labCurrentTemperature.text = [NSString stringWithFormat:@"%d", currentTemperature];
    self.labMinTemperature.text = [NSString stringWithFormat:@"%d°", minTemperature];
    self.labMaxTemperature.text = [NSString stringWithFormat:@"%d°", maxTemperature];
    
    // TODO: request yesterday's temperature history from server
    SensorChartView *yesterdayTemperatureChart = [[SensorChartView alloc] initWithFrame:CGRectMake(342, -4, 303, 169) DataType:kSensorTemperatureDataType];
    yesterdayTemperatureChart.minValue = 0;
    yesterdayTemperatureChart.maxValue = 100;
    yesterdayTemperatureChart.valueLevels = 5;
    yesterdayTemperatureChart.xAxisLabelSkips = 6;
    
    NSMutableArray *array = [NSMutableArray array];
    for(int i=0; i<=24; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *label = [NSString stringWithFormat:@"%d", i];
        [dict setObject:label forKey:@"label"];
        [dict setObject:[NSNumber numberWithInt:50] forKey:@"value"];
        [array addObject:dict];
    }
    yesterdayTemperatureChart.arrayChartValues = [NSArray arrayWithArray:array];
    [yesterdayTemperatureChart setNeedsDisplay];
    [self.scrollViewTemperture addSubview:yesterdayTemperatureChart];
    
    UILabel *labYesterday = [[UILabel alloc] initWithFrame:CGRectMake(480, 18, 140, 21)];
    labYesterday.backgroundColor = [UIColor clearColor];
    labYesterday.shadowColor = [UIColor clearColor];
    labYesterday.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
    labYesterday.font = [UIFont fontWithName:@"Helvetica-Light" size:10];
    labYesterday.textAlignment = NSTextAlignmentRight;
    labYesterday.text = @"2014/8/1";
    [self.scrollViewTemperture addSubview:labYesterday];

    // TODO: request today's temperature history from server
    SensorChartView *todayTemperatureChart = [[SensorChartView alloc] initWithFrame:CGRectMake(22, -4, 303, 169) DataType:kSensorTemperatureDataType];
    todayTemperatureChart.minValue = 0;
    todayTemperatureChart.maxValue = 100;
    todayTemperatureChart.valueLevels = 5;
    todayTemperatureChart.xAxisLabelSkips = 6;
    
    array = [NSMutableArray array];
    for(int i=0; i<=24; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *label = [NSString stringWithFormat:@"%d", i];
        [dict setObject:label forKey:@"label"];
        [dict setObject:[NSNumber numberWithInt:50] forKey:@"value"];
        [array addObject:dict];
    }
    todayTemperatureChart.arrayChartValues = [NSArray arrayWithArray:array];
    [todayTemperatureChart setNeedsDisplay];
    [self.scrollViewTemperture addSubview:todayTemperatureChart];
    
    UILabel *labToday = [[UILabel alloc] initWithFrame:CGRectMake(160, 18, 140, 21)];
    labToday.backgroundColor = [UIColor clearColor];
    labToday.shadowColor = [UIColor clearColor];
    labToday.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
    labToday.font = [UIFont fontWithName:@"Helvetica-Light" size:10];
    labToday.textAlignment = NSTextAlignmentRight;
    labToday.text = @"2014/8/2";
    [self.scrollViewTemperture addSubview:labToday];
}

-(void)requestHumidityData {
    
    // TODO: request current humidity data from server
    int currentHumidity = 75;
    int minHumidity = 60;
    int maxHumidity = 80;
    
    self.labCurrentHumidity.text = [NSString stringWithFormat:@"%d", currentHumidity];
    self.labMinHumidity.text = [NSString stringWithFormat:@"%d%c", minHumidity, 37];
    self.labMaxHumidity.text = [NSString stringWithFormat:@"%d%c", maxHumidity, 37];
    

    // TODO: request yesterday's humidity history from server
    SensorChartView *yesterdayHumidityChart = [[SensorChartView alloc] initWithFrame:CGRectMake(342, -4, 303, 169) DataType:kSensorHumidityDataType];
    yesterdayHumidityChart.minValue = 0;
    yesterdayHumidityChart.maxValue = 100;
    yesterdayHumidityChart.valueLevels = 5;
    yesterdayHumidityChart.xAxisLabelSkips = 6;
    
    NSMutableArray *array = [NSMutableArray array];
    for(int i=0; i<=24; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *label = [NSString stringWithFormat:@"%d", i];
        [dict setObject:label forKey:@"label"];
        [dict setObject:[NSNumber numberWithInt:50] forKey:@"value"];
        [array addObject:dict];
    }
    yesterdayHumidityChart.arrayChartValues = [NSArray arrayWithArray:array];
    [yesterdayHumidityChart setNeedsDisplay];
    [self.scrollViewHumidity addSubview:yesterdayHumidityChart];
    
    UILabel *labYesterday = [[UILabel alloc] initWithFrame:CGRectMake(480, 18, 140, 21)];
    labYesterday.backgroundColor = [UIColor clearColor];
    labYesterday.shadowColor = [UIColor clearColor];
    labYesterday.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
    labYesterday.font = [UIFont fontWithName:@"Helvetica-Light" size:10];
    labYesterday.textAlignment = NSTextAlignmentRight;
    labYesterday.text = @"2014/8/1";
    [self.scrollViewHumidity addSubview:labYesterday];
    
    
    // TODO: request today's humidity history from server
    SensorChartView *todayHumidityChart = [[SensorChartView alloc] initWithFrame:CGRectMake(22, -4, 303, 169) DataType:kSensorHumidityDataType];
    todayHumidityChart.minValue = 0;
    todayHumidityChart.maxValue = 100;
    todayHumidityChart.valueLevels = 5;
    todayHumidityChart.xAxisLabelSkips = 6;
    
    array = [NSMutableArray array];
    for(int i=0; i<=24; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *label = [NSString stringWithFormat:@"%d", i];
        [dict setObject:label forKey:@"label"];
        [dict setObject:[NSNumber numberWithInt:50] forKey:@"value"];
        [array addObject:dict];
    }
    todayHumidityChart.arrayChartValues = [NSArray arrayWithArray:array];
    [todayHumidityChart setNeedsDisplay];
    [self.scrollViewHumidity addSubview:todayHumidityChart];
    
    UILabel *labToday = [[UILabel alloc] initWithFrame:CGRectMake(160, 18, 140, 21)];
    labToday.backgroundColor = [UIColor clearColor];
    labToday.shadowColor = [UIColor clearColor];
    labToday.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
    labToday.font = [UIFont fontWithName:@"Helvetica-Light" size:10];
    labToday.textAlignment = NSTextAlignmentRight;
    labToday.text = @"2014/8/2";
    [self.scrollViewHumidity addSubview:labToday];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    if(scrollView.tag==1)
        self.pageControlTemperture.currentPage = page;
    else if(scrollView.tag==2)
        self.pageControlHumidity.currentPage = page;
}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pressBtnQueryTempetureHistory:(id)sender {
    QuerySensorHistoryViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"query_sensor_history"];
    vc.device = self.device;
    vc.sensorDataType = kSensorTemperatureDataType;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pressBtnQueryHumidityHistory:(id)sender {
    QuerySensorHistoryViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"query_sensor_history"];
    vc.device = self.device;
    vc.sensorDataType = kSensorHumidityDataType;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
