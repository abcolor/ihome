//
//  QuerySensorHistoryViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/25.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "QuerySensorHistoryViewController.h"
#import "DatePickerObject.h"
#import "IndicatorObject.h"

@interface QuerySensorHistoryViewController () {
    DateComponent beginDate;
    DateComponent endDate;
    DateComponent sampleTime;
    SensorChartView *sensorChart;
}
@property (weak, nonatomic) IBOutlet UIButton *btnQuery;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UILabel *labDeviceName;
@property (weak, nonatomic) IBOutlet UIButton *btnBeginDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSampleTime;
@end

@implementation QuerySensorHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:date];
    int currentYear = [components year];
    int currentMonth = [components month];
    int currentDay = [components day];
    int currentHour = [components hour];
    int currentMinute = [components minute];
    DateComponent currentDate = DateComponentMake(currentYear, currentMonth, currentDay, currentHour, currentMinute);
    beginDate = currentDate;
    endDate = currentDate;
    sampleTime = currentDate;

    [self setupUserInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupUserInterface {
    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // title
    if(self.sensorDataType==kSensorTemperatureDataType)
        self.navigationItem.title = @"查詢溫度記錄";
    else
        self.navigationItem.title = @"查詢濕度記錄";
    
    // device name
    self.labDeviceName.text = self.device.deviceName;
    
    // query date
    [self.btnBeginDate setTitle:[NSString stringWithFormat:@"%d/%d/%d", beginDate.year, beginDate.month, beginDate.day] forState:UIControlStateNormal];
    [self.btnEndDate setTitle:[NSString stringWithFormat:@"%d/%d/%d", endDate.year, endDate.month, endDate.day] forState:UIControlStateNormal];
    [self.btnSampleTime setTitle:[NSString stringWithFormat:@"%d:%02d", sampleTime.hour, sampleTime.minute] forState:UIControlStateNormal];
}

-(void)showQueryButton {
    self.btnQuery.hidden = NO;
    self.viewBackground.frame = CGRectMake(0, 239, self.viewBackground.frame.size.width, self.viewBackground.frame.size.height);
    if(sensorChart) [sensorChart removeFromSuperview];    
}

-(void)hideQueryButton {
    self.btnQuery.hidden = YES;
    self.viewBackground.frame = CGRectMake(0, 395, self.viewBackground.frame.size.width, self.viewBackground.frame.size.height);
}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pressBtnSelectBeginDate:(id)sender {
    
    [DatePickerObject showWithView:self.view Type:kDatePickerTypeDateOnly InitialDate:beginDate Completion:^(BOOL cancel, DateComponent date){
        if(!cancel) {
            beginDate = date;
            [self.btnBeginDate setTitle:[NSString stringWithFormat:@"%d/%d/%d", date.year, date.month, date.day] forState:UIControlStateNormal];
            [self showQueryButton];
        }
    }];
}

- (IBAction)pressBtnSelectEndDate:(id)sender {
    [DatePickerObject showWithView:self.view Type:kDatePickerTypeDateOnly InitialDate:endDate Completion:^(BOOL cancel, DateComponent date){
        if(!cancel) {
            endDate = date;
            [self.btnEndDate setTitle:[NSString stringWithFormat:@"%d/%d/%d", date.year, date.month, date.day] forState:UIControlStateNormal];
            [self showQueryButton];
        }
    }];
}

- (IBAction)pressBtnSelectTime:(id)sender {
    [DatePickerObject showWithView:self.view Type:kDatePickerTypeTimeOnly InitialDate:sampleTime Completion:^(BOOL cancel, DateComponent date){
        if(!cancel) {
            sampleTime = date;
            [self.btnSampleTime setTitle:[NSString stringWithFormat:@"%d:%02d", date.hour, date.minute] forState:UIControlStateNormal];
            [self showQueryButton];
        }
    }];
}

- (IBAction)pressBtnQuery:(id)sender {
    
    IndicatorObject *waitIndicator = [[IndicatorObject alloc] initWithView:self.view];
    [waitIndicator show];
    
    // TODO: integration
    // 1. request sensor data with sensorDataType, beginDate, endDate and sampleTime
    
    
    // 2. sensor data received, show chart
    [waitIndicator hide];
    
    sensorChart = [[SensorChartView alloc] initWithFrame:CGRectMake(22, 175, 298, 210) DataType:self.sensorDataType];
    
#if 1
    // test...
    sensorChart.minValue = 0;
    sensorChart.maxValue = 100;
    sensorChart.valueLevels = 5;
    sensorChart.xAxisLabelSkips = 10;
    
    NSMutableArray *array = [NSMutableArray array];
    for(int i=1; i<=31; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *label = [NSString stringWithFormat:@"8/%d", i];
        [dict setObject:label forKey:@"label"];
        [dict setObject:[NSNumber numberWithInt:50] forKey:@"value"];
        [array addObject:dict];
    }
    sensorChart.arrayChartValues = [NSArray arrayWithArray:array];
    [sensorChart setNeedsDisplay];
#endif
    
    sensorChart.viewDetailsEnabled = YES;
    [self.view addSubview:sensorChart];
    [self hideQueryButton];
}

@end
