//
//  UserPreferencesObject.h
//  iHome
//
//  Created by Liu David on 2014/8/4.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserPreferencesObject : NSObject
+(void)writeUserPreference:(NSString*)preference WithStringValue:(NSString*)value;
+(void)writeUserPreference:(NSString*)preference WithDictValue:(NSDictionary *)dictValue;
+(void)writeUserPreference:(NSString*)preference WithBoolValue:(BOOL)value;
+(NSString*)readStringValueOfUserPreference:(NSString*)preference;
+(NSDictionary*)readDictValueOfUserPreference:(NSString*)preference;
+(BOOL)readBoolValueOfUserPreference:(NSString*)preference;
+(void)clearUserPreference:(NSString*)preference;
@end
