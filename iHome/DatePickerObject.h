//
//  DatePickerObject.h
//  iHome
//
//  Created by Liu David on 2014/8/26.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kDatePickerTypeDateOnly,
    kDatePickerTypeTimeOnly,
    kDatePickerTypeDateAndTime
} DatePickerType;

struct DateComponent {
    NSInteger year;
    NSInteger month;
    NSInteger day;
    NSInteger hour;
    NSInteger minute;
};
typedef struct DateComponent DateComponent;

CG_INLINE DateComponent
DateComponentMake(NSInteger year, NSInteger month, NSInteger day, NSInteger hour, NSInteger minute)
{
    DateComponent date;
    date.year = year;
    date.month = month;
    date.day = day;
    date.hour = hour;
    date.minute = minute;
    return date;
}

typedef void(^DatePickerCompletionBlock)(BOOL cancel, DateComponent date);

@interface DatePickerObject : NSObject

+(void)showWithView:(UIView*)view Type:(DatePickerType)type InitialDate:(DateComponent)initialDate Completion:(DatePickerCompletionBlock)completion;

@end
