//
//  DeviceManagerObject.h
//  iHome
//
//  Created by Liu David on 2014/8/8.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceObject.h"

@interface DeviceManagerObject : NSObject

+(DeviceManagerObject*)sharedDeviceManagerObject;

-(void)addDevice:(DeviceObject*)device;
-(DeviceObject*)getDeviceWithId:(NSString*)deviceId;
-(void)updateDevice:(DeviceObject*)device;
-(void)deleteDevice:(DeviceObject*)device;

@property(readonly)int deviceNumber;
@property(readonly)NSArray *arrayDevices;

@end
