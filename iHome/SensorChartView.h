//
//  SensorChartView.h
//  iHome
//
//  Created by Liu David on 2014/8/25.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "ChartView.h"

typedef enum {
    kSensorTemperatureDataType,
    kSensorHumidityDataType
} SensorDataType;

@interface SensorChartView : ChartView
-(id)initWithFrame:(CGRect)frame DataType:(SensorDataType)dataType;
@end
