//
//  AppDelegate.h
//  iHome
//
//  Created by Liu David on 2014/8/1.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
