//
//  EventCell.m
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

@synthesize event = _event;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties


-(void)setEvent:(EventObject *)event {
    _event = event;    
    self.labEventDescription.text = event.description;
}

@end
