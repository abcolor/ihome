//
//  WebService.m
//  iHome
//
//  Created by Liu David on 2014/8/14.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "WebService.h"
#import "ASIFormDataRequest.h"
#import "UserPreferencesObject.h"

@implementation WebService

+(void)registerWithEmail:(NSString*)email Password:(NSString*)password UserName:(NSString*)userName Phone:(NSString*)phone Mobile:(NSString*)mobile Address:(NSString*)address Completion:(WebServiceCompletionBlockWithObject)completion {
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/%@/%@", AAA_SERVICE_URL, email, AAA_SERVICE_API_REGISTER];
    NSURL *url = [NSURL URLWithString: strUrl];
    
    
    
    __weak ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.requestMethod = @"POST";
    [request setPostValue:password forKey:@"passwd"];
    [request setPostValue:userName forKey:@"nickname"];
    [request setPostValue:phone forKey:@"phone"];
    [request setPostValue:mobile forKey:@"mobile"];
    [request setPostValue:address forKey:@"address"];
    [request setPostValue:AAA_SERVICE_DEFAULT_LANGUAGE forKey:@"default_lang"];
    [request setPostValue:AAA_SERVICE_DEFAULT_USER_TYPE forKey:@"user_type"];


    [request setCompletionBlock:^{
        if(completion) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:nil];
            NSString *uuid = [dict objectForKey:@"uuid"];
            NSLog(@"registerWithEmail - uuid: %@", uuid);
            completion(uuid, nil);
        }
    }];
    
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"registerWithEmail Error: %@", error.localizedDescription);
        if(completion) completion(nil, error);
    }];
    
    [request setNumberOfTimesToRetryOnTimeout:REQUEST_SERVICE_TIMEOUT_RETRY_NUMBER];
    [request startAsynchronous];
}


+(void)signinWithEmail:(NSString*)email Password:(NSString*)password Completion:(WebServiceCompletionBlockWithObject)completion {

    NSURL *url = [NSURL URLWithString: AAA_SERVICE_API_URL(AAA_SERVICE_API_SIGNIN)];
    
    __weak ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.requestMethod = @"POST";
    [request setPostValue:email forKey:@"username"];
    [request setPostValue:password forKey:@"password"];

    [request setCompletionBlock:^{
        if(completion) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            
            NSLog(@"signinWithEmail: %@", dict);

            NSString *error = [dict objectForKey:@"error_description"];
            if(!error) {
                completion(dict, nil);
            }
            else {
                completion(nil, [NSError errorWithDomain:error code:0 userInfo:nil]);
            }
        }
    }];
    
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"signinWithEmail Error: %@", error.localizedDescription);
        if(completion) completion(nil, error);
    }];
    
    [request setNumberOfTimesToRetryOnTimeout:REQUEST_SERVICE_TIMEOUT_RETRY_NUMBER];
    [request startAsynchronous];
}

+(void)signOutWithCompletion:(WebServiceCompletionBlock)completion {
    NSURL *url = [NSURL URLWithString: AAA_SERVICE_API_URL(AAA_SERVICE_API_SIGNOUT)];
    
    NSDictionary *dictUserInfo = [UserPreferencesObject readDictValueOfUserPreference:USER_SIGNIN_INFO];
    NSString *accessToken = [dictUserInfo objectForKey:@"access_token"];
    
    __weak ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Authorization: Bearer" value:accessToken];
    request.requestMethod = @"POST";
    
    [request setCompletionBlock:^{
        if(completion) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            
            NSLog(@"signOutWithCompletion: %@", dict);
            
            // TODO: should check response data
            completion(nil);
        }
    }];
    
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"signinWithEmail Error: %@", error.localizedDescription);
        if(completion) completion(error);
    }];
    
    [request setNumberOfTimesToRetryOnTimeout:REQUEST_SERVICE_TIMEOUT_RETRY_NUMBER];
    [request startAsynchronous];
}

@end
