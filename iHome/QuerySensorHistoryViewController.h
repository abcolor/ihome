//
//  QuerySensorHistoryViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/25.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"
#import "SensorChartView.h"

@interface QuerySensorHistoryViewController : UIViewController
@property(retain)DeviceObject *device;
@property(assign)SensorDataType sensorDataType;
@end
