//
//  SignInViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/4.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "SignInViewController.h"
#import "UserPreferencesObject.h"
#import "WebService.h"
#import "IndicatorObject.h"

@interface SignInViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtServerName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveSigninInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoSignin;
@end

@implementation SignInViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.txtServerName.delegate = self;
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    self.txtPassword.secureTextEntry = YES;
    
    // restore settings from user preference
    self.btnSaveSigninInfo.selected = [UserPreferencesObject readBoolValueOfUserPreference:PREFERENCES_SAVE_SIGNIN_INFO];
    self.btnAutoSignin.selected = [UserPreferencesObject readBoolValueOfUserPreference:PREFERENCES_AUTO_SIGNIN];
    
    if(self.btnSaveSigninInfo.selected) {
        self.txtServerName.text = [UserPreferencesObject readStringValueOfUserPreference:USER_SIGNIN_SERVER_NAME];
        self.txtEmail.text = [UserPreferencesObject readStringValueOfUserPreference:USER_SIGNIN_EMAIL];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(BOOL)checkUserSigninData {
    
    if(![self.txtEmail.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入電子郵件" Message:nil];
        return NO;
    }
    else if(![self.txtPassword.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入密碼" Message:nil];
        return NO;
    }
    else if(![self.txtServerName.text length]) {
        [IndicatorObject showDialogWithTitle:@"請輸入伺服器名稱" Message:nil];
        return NO;
    }
    
    return YES;
}


#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - IBActions

- (IBAction)pressBtnCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressBtnSaveSigninInfo:(id)sender {
    self.btnSaveSigninInfo.selected = !self.btnSaveSigninInfo.selected;
    [UserPreferencesObject writeUserPreference:PREFERENCES_SAVE_SIGNIN_INFO WithBoolValue:self.btnSaveSigninInfo.selected];
}

- (IBAction)pressBtnAutoSignin:(id)sender {
    self.btnAutoSignin.selected = !self.btnAutoSignin.selected;
    [UserPreferencesObject writeUserPreference:PREFERENCES_AUTO_SIGNIN WithBoolValue:self.btnAutoSignin.selected];
}

- (IBAction)pressBtnForgetPassword:(id)sender {
}

- (IBAction)pressBtnRegister:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate signInViewControllerDidSelectRegister];
    }];
}

- (IBAction)pressBtnSignin:(id)sender {
    
    if([self checkUserSigninData]) {
        
        // store user signin data
        if(self.btnSaveSigninInfo.selected) {
            [UserPreferencesObject writeUserPreference:USER_SIGNIN_SERVER_NAME WithStringValue:self.txtServerName.text];
            [UserPreferencesObject writeUserPreference:USER_SIGNIN_EMAIL WithStringValue:self.txtEmail.text];
        }
        else {
            [UserPreferencesObject clearUserPreference:USER_SIGNIN_SERVER_NAME];
            [UserPreferencesObject clearUserPreference:USER_SIGNIN_EMAIL];
        }
        
        // display wait indication
        IndicatorObject *waitIndicator = [[IndicatorObject alloc] initWithView:self.view];
        [waitIndicator show];
        
        WebServiceCompletionBlockWithObject signInCompletion = ^(NSDictionary *dictUserInfo, NSError *error){
            [waitIndicator hide];
            if(!error) {                              
                // store user's access token
                [UserPreferencesObject writeUserPreference:USER_SIGNIN_INFO WithDictValue:dictUserInfo];
                
                // dismiss & switch to main menu
                [self dismissViewControllerAnimated:YES completion:^{
                    [self.delegate signInViewControllerDidCompleted];                    
                }];
            }
            else {
                [IndicatorObject showDialogWithTitle:@"登入失敗" Message:error.description];
            }
        };
        
        // AAA signin
        [WebService signinWithEmail:self.txtEmail.text
                           Password:self.txtPassword.text
                         Completion:signInCompletion];
    }
}

@end
