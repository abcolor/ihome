//
//  SensorCell.m
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "SensorCell.h"

@implementation SensorCell

@synthesize device = _device;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties

-(void)setDevice:(DeviceObject *)device {
    _device = device;
    
    self.labDeviceName.text = device.deviceName;
    
    // TODO: request device's sensor data
    int temperature = 25;
    int humidity = 75;
    
    self.labTemperature.text = [NSString stringWithFormat:@"%d°", temperature];
    self.labHumidity.text = [NSString stringWithFormat:@"%d%c", humidity, 37];
}

@end
