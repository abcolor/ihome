//
//  DeviceInfoViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "DeviceInfoViewController.h"
#import "DeviceManagerObject.h"
#import "UIAlertView+NSCookbook.h"

@interface DeviceInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labDeviceName;
@property (weak, nonatomic) IBOutlet UILabel *labTemperture;
@property (weak, nonatomic) IBOutlet UILabel *labHumidity;
@property (weak, nonatomic) IBOutlet UISwitch *switchVideoStreamEnabled;
@property (weak, nonatomic) IBOutlet UISwitch *switchAudioStreamEnabled;
@end

@implementation DeviceInfoViewController

@synthesize device;

- (void)viewDidLoad
{
    [super viewDidLoad];

    // left navigation button (toggle side bar menu)
    UIImage *img = [UIImage imageNamed:@"back"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake( 0, 0, img.size.width, img.size.height );
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    // title
    self.navigationItem.title = self.device.deviceName;
    
    // device info
    self.labDeviceName.text = self.device.deviceName;
    self.switchVideoStreamEnabled.on = self.device.videoStreamEnabled;
    self.switchAudioStreamEnabled.on = self.device.audioStreamEnabled;
    
    UIColor *color = [UIColor colorWithRed:0.96f green:0.05 blue:0.21 alpha:1];
    self.switchVideoStreamEnabled.onTintColor = color;
    self.switchAudioStreamEnabled.onTintColor = color;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(self.device)
        [[DeviceManagerObject sharedDeviceManagerObject] updateDevice:self.device];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - IBActions

-(void)pressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeVideoStreamEnableValue:(id)sender {
    self.device.videoStreamEnabled = self.switchVideoStreamEnabled.on;
}

- (IBAction)changeAudioStreamEnableValue:(id)sender {
    self.device.audioStreamEnabled = self.switchAudioStreamEnabled.on;
}

- (IBAction)pressBtnDeleteDevice:(id)sender {
    
    UIAlertView *prompt = [UIAlertView alloc];
    prompt = [prompt initWithTitle:@"刪除裝置"
                           message:@"你確定要刪除此裝置嗎?"
                          delegate:nil
                 cancelButtonTitle:@"取消"
                 otherButtonTitles: @"確定", nil];
    
    [prompt showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1) { // delete device
            [[DeviceManagerObject sharedDeviceManagerObject] deleteDevice:self.device];
            self.device = nil;
            [self.navigationController popViewControllerAnimated:YES];        }
    }];
}


@end
