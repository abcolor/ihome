//
//  EventManagerObject.m
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "EventManagerObject.h"
#import "FileManagementHelper.h"
#import "FMDB.h"

@interface EventManagerObject()
@property(retain)FMDatabase *db;
@end


@implementation EventManagerObject

#pragma mark - Class Methods

static EventManagerObject *sharedEventManagerObject = nil;

+(id)sharedEventManagerObject {
	// synchronized is used to lock the object and handle multiple threads accessing this method at
	// the same time
	@synchronized(self) {
		
		// If the sharedSoundManager var is nil then we need to allocate it.
		if(sharedEventManagerObject == nil) {
			// Allocate and initialize an instance of this class
			sharedEventManagerObject = [[self alloc] init];
		}
	}
    return sharedEventManagerObject;
}


+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedEventManagerObject == nil) {
            sharedEventManagerObject = [super allocWithZone:zone];
            return sharedEventManagerObject;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}


#pragma mark - Instance Methods

-(id)init {
    if(self = [super init]) {
        
        NSString *dbPath = [FileManagementHelper getPathWithName:@"event.db"];
        
        self.db = [FMDatabase databaseWithPath:dbPath] ;
        
        if ([self.db open]) {
            NSLog(@"event db opened");
            
            if(![self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS EventList (Id text, Description text, Unread integer)"]) {
                NSLog(@"Could not create table: %@", [self.db lastErrorMessage]);
            }
        }
    }
    return self;
}

-(void)addEvent:(EventObject*)event {
    BOOL rs = [self.db executeUpdate:@"INSERT INTO EventList (Id, Description, Unread) VALUES (?,?,?)",
               event.eventId,
               event.description,
               [NSNumber numberWithBool:event.unread] ];
    
    if(!rs) NSLog(@"addEvent fail!");
}

-(void)updateEvent:(EventObject*)event {
    [self.db executeUpdate:@"UPDATE EventList SET Description = ? WHERE Id = ?", event.description, event.eventId];
    [self.db executeUpdate:@"UPDATE EventList SET Unread = ? WHERE Id = ?", [NSNumber numberWithBool:event.unread], event.eventId];
}


-(void)deleteEvent:(EventObject*)event {
    if(![self.db executeUpdate:@"DELETE FROM EventList WHERE Id = ?", event.eventId]) {
        NSLog(@"Could not delete event: %@", event.eventId);
    }
}

-(void)deleteAllItems {
    for(EventObject *event in [self.arrayEvents objectEnumerator]) {
        [self deleteEvent:event];
    }
}

#pragma mark - Properties

-(int)eventNumber {
    return (int)[self.db intForQuery:@"SELECT COUNT(*) FROM EventList"];
}

-(NSArray*)arrayEvents {
    
    NSMutableArray *array = [NSMutableArray array];
    FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM EventList"];
    while ([rs next]) {
        EventObject *event = [[EventObject alloc] init];
        event.eventId = [rs stringForColumn:@"Id"];
        event.description = [rs stringForColumn:@"Description"];
        event.unread = [rs intForColumn:@"Unread"];
        [array addObject:event];
    }    
    return [NSArray arrayWithArray:array];
}


@end
