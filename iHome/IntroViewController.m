//
//  IntroViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/1.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "IntroViewController.h"
#import "IndicatorObject.h"
#import "SignInViewController.h"
#import "RegisterViewController.h"
#import "UserPreferencesObject.h"

@interface IntroViewController () <UIScrollViewDelegate, SignInViewControllerDelegate, RegisterViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@end

@implementation IntroViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupIntroScrollViewContents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)setupIntroScrollViewContents {
    NSArray *titleText = @[@"智慧家庭", @"遠端監控", @"環境監控", @"智慧家電"];
    NSArray *descriptionText = @[@"讓你的房子 變得更聰明", @"隨時可以從任何地方觀看屋內環境", @"讓你更了解屋內環境的變化", @"讓家中電器變得更聰明"];
    NSArray *backgroundImageName = @[@"intro_1", @"intro_2", @"intro_3", @"intro_4"];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [backgroundImageName count], self.scrollView.frame.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    
    int offset = 0;
    for(int i=0; i < [backgroundImageName count]; i++) {
        
        // icon
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[backgroundImageName objectAtIndex:i]]];
        [self.scrollView addSubview:img];
        img.frame = CGRectMake(offset, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        
        // title
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(offset + 105, 240, 110, 35)];
        title.backgroundColor = [UIColor clearColor];
        title.shadowColor = [UIColor clearColor];
        title.textColor = [UIColor whiteColor];
        title.font = [UIFont fontWithName:@"Helvetica-Light" size:22];
        title.textAlignment = NSTextAlignmentCenter;
        title.text = [titleText objectAtIndex:i];
        [self.scrollView addSubview:title];
        
        // description
        UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(offset, 280, 320, 35)];
        description.backgroundColor = [UIColor clearColor];
        description.shadowColor = [UIColor clearColor];
        description.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.85];
        description.font = [UIFont fontWithName:@"Helvetica-Light" size:16];
        description.textAlignment = NSTextAlignmentCenter;
        description.text = [descriptionText objectAtIndex:i];
        [self.scrollView addSubview:description];
        
        offset+=self.scrollView.frame.size.width;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_signin"]) {
        SignInViewController *signinVc = (SignInViewController*)segue.destinationViewController;
        signinVc.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"segue_register"]) {
        RegisterViewController *registerVc = (RegisterViewController*)segue.destinationViewController;
        registerVc.delegate = self;
        //[segue.destinationViewController setValue:sender forKey:@"imgRandomNumber"];
    }
}

#pragma mark - SignInViewControllerDelegate

-(void)signInViewControllerDidSelectRegister {
    [self performSegueWithIdentifier:@"segue_register" sender:self];
}

-(void)signInViewControllerDidCompleted {
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"main_functions"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - RegisterViewControllerDelegate

-(void)registerViewControllerDidCompleted {    
    [self performSegueWithIdentifier:@"segue_signin" sender:self];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    self.pageControl.currentPage = page;
}

#pragma mark - IBActions

- (IBAction)pressBtnRegister:(id)sender {
    IndicatorObject *waitIndicator = [[IndicatorObject alloc] initWithView:self.view];
    [waitIndicator show];
    
    // TODO: request a random number image from server
    
    [waitIndicator hide];
    
    //UIImage *imgRandomNumber = nil;
    //[self performSegueWithIdentifier:@"segue_register" sender:imgRandomNumber];
    [self performSegueWithIdentifier:@"segue_register" sender:sender];
}

- (IBAction)pressBtnSignin:(id)sender {
    [self performSegueWithIdentifier:@"segue_signin" sender:sender];
}

@end
