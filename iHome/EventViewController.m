//
//  EventViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "EventViewController.h"
#import "EventManagerObject.h"
#import "EventCell.h"
#import "UIAlertView+NSCookbook.h"

@interface EventViewController () {
    EventManagerObject *eventManager;
}
@end

@implementation EventViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    eventManager = [EventManagerObject sharedEventManagerObject];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"清除" style:UIBarButtonItemStylePlain target:self action:@selector(pressBtnDeleteAllEvents:)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // update all unread event status
    for(EventObject *event in [eventManager.arrayEvents objectEnumerator]) {
        if(event.unread) {
            event.unread = NO;
            [eventManager updateEvent:event];
        }
    }
    
    // clear event tab badge
    [self clearEventBadge];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [eventManager eventNumber];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"event_cell";
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.event = [eventManager.arrayEvents objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        EventObject *event = [eventManager.arrayEvents objectAtIndex:indexPath.row];
        [eventManager deleteEvent:event];
        [self.tableView reloadData];
    }
}

#pragma mark -

-(void)clearEventBadge {
    UITabBarItem *notificationBarItem = [[self.tabBarController.viewControllers objectAtIndex:3] tabBarItem];
    notificationBarItem.badgeValue = nil;
}

#pragma mark - Button Handler

- (void)pressBtnDeleteAllEvents:(id)sender {

    UIAlertView *prompt = [UIAlertView alloc];
    prompt = [prompt initWithTitle:@"清除事件"
                           message:@"你確定要清除事件列表嗎?"
                          delegate:nil
                 cancelButtonTitle:@"取消"
                 otherButtonTitles: @"確定", nil];
    
    [prompt showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1) { // delete all notification items
            [eventManager deleteAllItems];
            [self.tableView reloadData];
        }
    }];
}

@end
