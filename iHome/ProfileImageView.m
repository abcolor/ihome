//
//  ProfileImageView.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "ProfileImageView.h"

@implementation ProfileImageView

@synthesize imgProfile = _imgProfile;

#pragma mark - Properties

-(void)setImgProfile:(UIImage *)imgProfile {
    _imgProfile = imgProfile;
    
    if(imgProfile) {
        self.image = imgProfile;
        [self.layer setCornerRadius:self.frame.size.width / 2.0f];
        [self.layer setBorderWidth:0];
        [self.layer setMasksToBounds:YES];
    }
}

@end
