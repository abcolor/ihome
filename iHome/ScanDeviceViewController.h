//
//  ScanDeviceViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/15.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScanDeviceViewControllerDelegate <NSObject>
-(void)scanDeviceViewControllerDidFoundInfo:(NSString*)foundInfo;
@end

@interface ScanDeviceViewController : UIViewController
@property(nonatomic, assign) id<ScanDeviceViewControllerDelegate> delegate;
@end
