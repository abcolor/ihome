//
//  DeviceInfoViewController.h
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceObject.h"

@interface DeviceInfoViewController : UITableViewController
@property(retain)DeviceObject *device;
@end
