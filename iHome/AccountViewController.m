//
//  AccountViewController.m
//  iHome
//
//  Created by Liu David on 2014/8/5.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "AccountViewController.h"
#import "WebService.h"
#import "UserPreferencesObject.h"
#import "IndicatorObject.h"

@interface AccountViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labUserName;
@property (weak, nonatomic) IBOutlet UILabel *labEmail;
@end

@implementation AccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSDictionary *dictUserInfo = [UserPreferencesObject readDictValueOfUserPreference:USER_SIGNIN_INFO];
    self.labUserName.text = [dictUserInfo objectForKey:@"nickname"];
    self.labEmail.text = [dictUserInfo objectForKey:@"username"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtnSignout:(id)sender {
    
    // display wait indication
    IndicatorObject *waitIndicator = [[IndicatorObject alloc] initWithView:self.view];
    [waitIndicator show];
    
    [WebService signOutWithCompletion:^(NSError *error){
        [waitIndicator hide];
        
        // clear user's signin info
        [UserPreferencesObject clearUserPreference:USER_SIGNIN_INFO];
        
        // switch to intro page
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"Signin" bundle:nil] instantiateViewControllerWithIdentifier:@"sign_in"];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:vc animated:YES completion:nil];
    }];
}

@end
