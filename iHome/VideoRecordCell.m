//
//  VideoRecordCell.m
//  iHome
//
//  Created by Liu David on 2014/8/27.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "VideoRecordCell.h"

@interface VideoRecordCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgVideoPreview;
@property (weak, nonatomic) IBOutlet UILabel *labVideoDate;
@end

@implementation VideoRecordCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



#pragma mark - Properties

-(void)setVideoRecord:(VideoRecordObject *)videoRecord {

    _videoRecord = videoRecord;
    
    if(videoRecord.imgPreview) self.imgVideoPreview.image = videoRecord.imgPreview;
    self.labVideoDate.text = [NSString stringWithFormat:@"%d/%d/%d %d:%02d", videoRecord.date.year, videoRecord.date.month, videoRecord.date.day, videoRecord.date.hour, videoRecord.date.minute];    
}


@end
