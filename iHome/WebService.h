//
//  WebService.h
//  iHome
//
//  Created by Liu David on 2014/8/14.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^WebServiceCompletionBlock)(NSError *error);
typedef void(^WebServiceCompletionBlockWithObject)(id obj, NSError *error);

@interface WebService : NSObject

+(void)registerWithEmail:(NSString*)email Password:(NSString*)password UserName:(NSString*)userName Phone:(NSString*)phone Mobile:(NSString*)mobile Address:(NSString*)address Completion:(WebServiceCompletionBlockWithObject)completion;
+(void)signinWithEmail:(NSString*)email Password:(NSString*)password Completion:(WebServiceCompletionBlockWithObject)completion;
+(void)signOutWithCompletion:(WebServiceCompletionBlock)completion;
@end
