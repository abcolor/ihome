//
//  DatePickerObject.m
//  iHome
//
//  Created by Liu David on 2014/8/26.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "DatePickerObject.h"
//#import "SimplePickerSheet.h"
#import "PopupPicker.h"

@implementation DatePickerObject

static PopupPicker *sharedPicker = nil;

+(void)showWithView:(UIView*)view Type:(DatePickerType)type InitialDate:(DateComponent)initialDate Completion:(DatePickerCompletionBlock)completion {
    
    __block DateComponent selectedDate = initialDate;
    
    sharedPicker = [[PopupPicker alloc] initWithCompletion:^(PopupPicker *picker, BOOL cancelled, NSArray *selectedIndexValues) {
        if(completion) completion(cancelled, selectedDate);
    }];
    
    
    NSInteger index;
    if((type==kDatePickerTypeDateOnly)||(type==kDatePickerTypeDateAndTime)) {
        
        if(type==kDatePickerTypeDateOnly) {
            // year components
            NSMutableArray *arrayYear = [NSMutableArray array];
            for(int i=2010; i<=2020;i++) {
                [arrayYear addObject:[NSString stringWithFormat:@"%d年",i]];
            }
            index = [arrayYear indexOfObject:[NSString stringWithFormat:@"%d年", (int)initialDate.year]];
            [sharedPicker addComponent:arrayYear
                     initialSelection:(index > [arrayYear count])?0:(int)index
                              changed:^(PopupPicker *picker, NSArray *componentValues, int selectedItem) {
                                  //NSLog(@"wheel changed, new value: %@", componentValues[selectedItem]);
                                  selectedDate.year = selectedItem + 2010;
                              }];
        }
        
        // month components
        NSMutableArray *arrayMonth = [NSMutableArray array];
        for(int i=1; i<=12;i++) {
            [arrayMonth addObject:[NSString stringWithFormat:@"%d月",i]];
        }
        index = [arrayMonth indexOfObject:[NSString stringWithFormat:@"%d月", (int)initialDate.month]];
        //index = [arrayMonth indexOfObject:[NSString stringWithFormat:@"%d年", initialDate.mo]];
        [sharedPicker addComponent:arrayMonth
                 initialSelection:(index > [arrayMonth count])?0:(int)index
                          changed:^(PopupPicker *picker, NSArray *componentValues, int selectedItem) {
                              selectedDate.month = selectedItem + 1;
                              //NSLog(@"wheel changed, new value: %@", componentValues[selectedItem]);
                          }];
        
        // day components
        NSMutableArray *arrayDay = [NSMutableArray array];
        for(int i=1; i<=31;i++) {
            [arrayDay addObject:[NSString stringWithFormat:@"%d日",i]];
        }
        index = [arrayDay indexOfObject:[NSString stringWithFormat:@"%d日", (int)initialDate.day]];
        [sharedPicker addComponent:arrayDay
                 initialSelection:(index > [arrayDay count])?0:(int)index
                          changed:^(PopupPicker *picker, NSArray *componentValues, int selectedItem) {
                              //NSLog(@"wheel changed, new value: %@", componentValues[selectedItem]);
                              selectedDate.day = selectedItem + 1;
                          }];
    }

    if((type==kDatePickerTypeTimeOnly)||(type==kDatePickerTypeDateAndTime)) {
        
        // hour components
        NSMutableArray *arrayHour = [NSMutableArray array];
        for(int i=0; i<=23;i++) {
            [arrayHour addObject:[NSString stringWithFormat:@"%d",i]];
        }
        index = [arrayHour indexOfObject:[NSString stringWithFormat:@"%d", (int)initialDate.hour]];
        [sharedPicker addComponent:arrayHour
                 initialSelection:(index > [arrayHour count])?0:(int)index
                          changed:^(PopupPicker *picker, NSArray *componentValues, int selectedItem) {
                              //NSLog(@"wheel changed, new value: %@", componentValues[selectedItem]);
                              selectedDate.hour = selectedItem;
                          }];
        
        // minute components
        NSMutableArray *arrayMinute = [NSMutableArray array];
        for(int i=0; i<=59;i++) {
            [arrayMinute addObject:[NSString stringWithFormat:@"%d",i]];
        }
        index = [arrayMinute indexOfObject:[NSString stringWithFormat:@"%d", (int)initialDate.minute]];
        [sharedPicker addComponent:arrayMinute
                 initialSelection:(index > [arrayMinute count])?0:(int)index
                          changed:^(PopupPicker *picker, NSArray *componentValues, int selectedItem) {
                              //NSLog(@"wheel changed, new value: %@", componentValues[selectedItem]);
                              selectedDate.minute = selectedItem;
                          }];
    }
    
    [sharedPicker show];
}

@end
