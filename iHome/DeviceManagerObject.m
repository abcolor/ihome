//
//  DeviceManagerObject.m
//  iHome
//
//  Created by Liu David on 2014/8/8.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import "DeviceManagerObject.h"
#import "FileManagementHelper.h"
#import "FMDB.h"

@interface DeviceManagerObject()
@property(retain)FMDatabase *db;
@end

@implementation DeviceManagerObject

#pragma mark - Class Methods

static DeviceManagerObject *sharedDeviceManagerObject = nil;

+(id)sharedDeviceManagerObject {
	// synchronized is used to lock the object and handle multiple threads accessing this method at
	// the same time
	@synchronized(self) {
		
		// If the sharedSoundManager var is nil then we need to allocate it.
		if(sharedDeviceManagerObject == nil) {
			// Allocate and initialize an instance of this class
			sharedDeviceManagerObject = [[self alloc] init];
		}
	}
    return sharedDeviceManagerObject;
}


+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedDeviceManagerObject == nil) {
            sharedDeviceManagerObject = [super allocWithZone:zone];
            return sharedDeviceManagerObject;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}


#pragma mark - Instance Methods

-(id)init {
    if(self = [super init]) {                
        
        NSString *dbPath = [FileManagementHelper getPathWithName:@"device.db"];
        
        self.db = [FMDatabase databaseWithPath:dbPath] ;
        
        if ([self.db open]) {
            NSLog(@"device db opened");
            
            if(![self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS DeviceList (Name text, Id text, MAC text, VideoStreamEnabled integer, AudioStreamEnabled integer)"]) {
                NSLog(@"Could not create table: %@", [self.db lastErrorMessage]);
            }
        }
    }
    return self;
}

-(void)addDevice:(DeviceObject*)device {
    BOOL rs = [self.db executeUpdate:@"INSERT INTO DeviceList (Name, Id, MAC, VideoStreamEnabled, AudioStreamEnabled) VALUES (?,?,?,?,?)",
               device.deviceName,
               device.deviceId,
               device.macAddress,
               [NSNumber numberWithBool:device.videoStreamEnabled],
               [NSNumber numberWithBool:device.audioStreamEnabled] ];
    
    if(!rs) NSLog(@"addDevice fail!");
    
//     [NSNumber numberWithBool:device.audioStreamEnabled],
//     UIImageJPEGRepresentation(device.devicePhoto, 0.8f)];
}

-(DeviceObject*)getDeviceWithId:(NSString*)deviceId {    
    
    DeviceObject *device = nil;
    FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM DeviceList", deviceId];
    
    if(rs) {
        device = [[DeviceObject alloc] init];
        device.deviceId = deviceId;
        device.deviceName = [rs stringForColumn:@"Name"];
        device.macAddress = [rs stringForColumn:@"MAC"];
        device.videoStreamEnabled = [rs intForColumn:@"VideoStreamEnabled"];
        device.audioStreamEnabled = [rs intForColumn:@"AudioStreamEnabled"];
    }
    else {
        NSLog(@"Device Id: %@ not exist", deviceId);
    }
    
    return device;
}

-(void)updateDevice:(DeviceObject*)device {
    [self.db executeUpdate:@"UPDATE DeviceList SET Name = ? WHERE Id = ?", device.deviceName, device.deviceId];
    [self.db executeUpdate:@"UPDATE DeviceList SET MAC = ? WHERE Id = ?", device.macAddress, device.deviceId];
    [self.db executeUpdate:@"UPDATE DeviceList SET VideoStreamEnabled = ? WHERE Id = ?", [NSNumber numberWithBool:device.videoStreamEnabled], device.deviceId];
    [self.db executeUpdate:@"UPDATE DeviceList SET AudioStreamEnabled = ? WHERE Id = ?", [NSNumber numberWithBool:device.audioStreamEnabled], device.deviceId];
}

-(void)deleteDevice:(DeviceObject*)device {
    if(![self.db executeUpdate:@"DELETE FROM DeviceList WHERE Id = ?", device.deviceId]) {
        NSLog(@"Could not delete device: %@", device.deviceId);
    }
}


#pragma mark - Properties

-(int)deviceNumber {
    return (int)[self.db intForQuery:@"SELECT COUNT(*) FROM DeviceList"];
}

-(NSArray*)arrayDevices {
    
    NSMutableArray *array = [NSMutableArray array];
    FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM DeviceList"];
    while ([rs next]) {
        DeviceObject *device = [[DeviceObject alloc] init];
        device.deviceId = [rs stringForColumn:@"Id"];
        device.deviceName = [rs stringForColumn:@"Name"];
        device.macAddress = [rs stringForColumn:@"MAC"];
        device.videoStreamEnabled = [rs intForColumn:@"VideoStreamEnabled"];
        device.audioStreamEnabled = [rs intForColumn:@"AudioStreamEnabled"];
        [array addObject:device];
    }

    return [NSArray arrayWithArray:array];
}

@end
