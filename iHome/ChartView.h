//
//  ChartView.h
//  iHome
//
//  Created by Liu David on 2014/8/20.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CHART_VIEW_BOUNDARY 20

@interface ChartView : UIView

@property(assign)int minValue;
@property(assign)int maxValue;
@property(assign)int valueLevels;
@property(assign)int xAxisLabelSkips;
@property(retain)NSArray *arrayChartValues;
@property(retain)NSString *dataUnit;
@property(assign, nonatomic)BOOL viewDetailsEnabled;
@property(assign)BOOL chartLineHidden;
@end
