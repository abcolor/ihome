//
//  EventManagerObject.h
//  iHome
//
//  Created by Liu David on 2014/8/24.
//  Copyright (c) 2014年 Liu David. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventObject.h"

@interface EventManagerObject : NSObject

+(EventManagerObject*)sharedEventManagerObject;

-(void)addEvent:(EventObject*)event;
-(void)updateEvent:(EventObject*)event;
-(void)deleteEvent:(EventObject*)event;
-(void)deleteAllItems;

@property(readonly)int eventNumber;
@property(readonly)NSArray *arrayEvents;

@end
