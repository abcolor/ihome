//
//  SimplePickerSheet.m
//
//  Created by David Mojdehi on 6/18/13.
//  Copyright (c) 2013 Mindful Bear Apps. All rights reserved.
//

#import "SimplePickerSheet.h"
#import <objc/runtime.h>


const char *kSimplePickerButtonProc = "kSimplePickerButtonProc";


@interface SimplePickerSheet()
@property (nonatomic, strong) NSMutableArray *componentRowValues;
@property (nonatomic, strong) NSMutableArray *componentChangeBlocks;
@property (nonatomic, strong) NSObject *selfReference;
@property (nonatomic, assign) BOOL showCancelButton;
@property (nonatomic, strong) UIActionSheet *actionSheet;


@end


@implementation SimplePickerSheet

@synthesize selectedItem = _selectedItem;

-(id)initWithTitle:(NSString*)title andCancelButton:(BOOL)showCancelButton dismissed:(SimpleCompletionBlock)completionBlock
{
	if(self = [super init])
	{
		_completionBlock = [completionBlock copy];
		
		_showCancelButton = showCancelButton;
		_componentRowValues = [NSMutableArray array];
		_componentChangeBlocks = [NSMutableArray array];
		
		CGRect pickerFrame = CGRectMake(0, 40, self.viewSize.width, 300);
		_pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
		_pickerView.delegate = self;
		_pickerView.dataSource = self;
		_pickerView.showsSelectionIndicator = YES;
		
		// hold a reference to ourselves (nobody else does)
		_selfReference = self;
	}
	return self;
}

-(void)addComponent:(NSArray*)rowValues  initialSelection:(int)initialSelection changed:(ComponentChangedBlock)changedActionBlock;
{
	ComponentChangedBlock blockCopy = [changedActionBlock copy];
	[_componentRowValues addObject:rowValues];
	if(changedActionBlock)
		[_componentChangeBlocks addObject:blockCopy];
	else
		[_componentChangeBlocks addObject:[NSNull null]];
	
	[_pickerView reloadAllComponents];
	
	// scrolls the specified row to center.
	[_pickerView selectRow:initialSelection inComponent:_componentRowValues.count - 1 animated:NO];
    
    _selectedItem = rowValues[0];
}

-(void)showInView:(UIView*)view
{
	
	// we always show in the main window
	UIView *origin = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
	CGRect presentRect = CGRectMake(origin.center.x, origin.center.y, 1, 1);
	
    UIView *masterView = [self createPickerViewWrapper];
    _actionSheet = [self createActionSheet];
    [_actionSheet addSubview:masterView];
    
    CGRect r = _actionSheet.bounds;
    [_actionSheet showFromRect:presentRect inView:view animated:YES];
    
    // note: re-apply the bounds (showing it clobbers our custom value)
    _actionSheet.bounds = r;
}



#pragma mark Helper Methods

- (CGSize)viewSize {
    if (![self isViewPortrait])
        return CGSizeMake(480, 320);
    return CGSizeMake(320, 480);
}

- (BOOL)isViewPortrait {
    return UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
}

-(NSArray*)generateSelectedIndexes
{
	NSMutableArray *selectedVals = [NSMutableArray array];
	for(int i = 0; i< _componentRowValues.count; i++)
	{
		NSInteger selectedRow = [_pickerView selectedRowInComponent:i];
		[selectedVals addObject:[NSNumber numberWithInteger:selectedRow]];
	}
    
	return selectedVals;
}


-(UIView*)createPickerViewWrapper
{
	UIView *masterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.viewSize.width, 260)];
	CGRect frame = CGRectMake(8, 0, self.viewSize.width-16, 44);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor colorWithRed:0.7f green:0.7f blue:0.7f alpha:1];//CMDP_MENU_BACKGROUND_COLOR;
    [masterView addSubview:view];
    
    UIButton *btn;
    
    // add a cancel button?
	if (_showCancelButton) {
        
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"Cancel" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:18];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = CGRectMake(0, 0, 80, 44);
        [view addSubview:btn];
	}
    
    // add done button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"Done" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:18];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(320-90, 0, 80, 44);
    [view addSubview:btn];

		
    NSAssert(_pickerView != NULL, @"Picker view failed to instantiate, perhaps you have invalid component data.");
    [masterView addSubview:_pickerView];
		return masterView;
}


// UIActionSheet hacks:
//		- title:  wtf are the \n's for?
//		- bounds: we hack it into position after displaying it
//		- tap:	  we hack it for off-sheet taps to cancel
//		- no buttons:  we don't have any buttons on the action sheet
// So all we're using is the animation appearance??
//
// TODO: we hack up UIActionSheet so much we should probably just create our own view
-(UIActionSheet*)createActionSheet
{
	// figure out the title & size
	NSString *paddedSheetTitle = nil;
    CGFloat sheetHeight = self.viewSize.height - 47;
    if ([self isViewPortrait]) {
        paddedSheetTitle = @"\n\n\n"; // looks hacky to me
    } else {
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
            sheetHeight = self.viewSize.width;
        } else {
            sheetHeight += 103;
        }
    }
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:paddedSheetTitle
															 delegate:nil
													cancelButtonTitle:nil
											   destructiveButtonTitle:nil
													otherButtonTitles:nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
	
	
	// note: we set the bounds here, though they're clobbered when shown
    actionSheet.bounds = CGRectMake(0, 0, self.viewSize.width, sheetHeight);
	
	return actionSheet;
	
}

#pragma mark -
#pragma mark Button Handling


-(void)cancel
{
	// call the action proc first
	if(_completionBlock)
	{
		_completionBlock(self, YES, nil);
	}
	[self dismissSilently];
	
}

-(void)done
{
	// make sure the wheel isn't moving
	
	// call the action proc first
	if(_completionBlock)
	{
		NSArray *values = [self generateSelectedIndexes];
		_completionBlock(self, NO, values);
	}
	[self dismissSilently];
}

// hides the sheet or popover; does *not* call completion blocks
- (void)dismissSilently
{
	if (_actionSheet && [_actionSheet isVisible])
		[_actionSheet dismissWithClickedButtonIndex:0 animated:YES];

    _actionSheet = nil;
    _selfReference = nil;
}


#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return _componentRowValues.count;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSArray *rowValues = [_componentRowValues objectAtIndex:component];
	return rowValues.count;
}

#pragma mark -
#pragma mark UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	
	NSArray *rowValues = [_componentRowValues objectAtIndex:component];
	id rowValue = [rowValues objectAtIndex:row];
	NSString *titleForRow = [rowValue description];
	
	return titleForRow;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componentIdx
{
	// see if the caller wants notification of the new values
	id changedActionBlock = [_componentChangeBlocks objectAtIndex:componentIdx];
	
    NSArray *componentValues = [_componentRowValues objectAtIndex:componentIdx];
    //self.selectedItem = componentValues[row];

	if(changedActionBlock && ( changedActionBlock != [NSNull null]) )
	{
		ComponentChangedBlock changedBlock = (ComponentChangedBlock) changedActionBlock;
		changedBlock(self, componentValues, (int)row);
    }
}

#pragma mark - Properties

-(void)setSelectedItem:(NSString *)selectedItem {
    _selectedItem = selectedItem;
    NSArray *array = [_componentRowValues objectAtIndex:0];
    int index = (int)[array indexOfObject:selectedItem];
    [_pickerView selectRow:index inComponent:_componentRowValues.count - 1 animated:NO];
}

@end
