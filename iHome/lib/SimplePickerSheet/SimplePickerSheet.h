//
//  SimplePickerSheet.h
//
//  Created by David Mojdehi on 6/18/13.
//  Copyright (c) 2013 Mindful Bear Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SimplePickerSheet;


// When the picker sheet is dismissed, this block is called
typedef void(^SimpleCompletionBlock)(SimplePickerSheet *picker, BOOL cancelled, NSArray *selectedComponentIndexes);

// Individuall callback when a rotating wheel value changes
typedef void(^ComponentChangedBlock)(SimplePickerSheet *picker, NSArray *componentValues, int selectedItem);

// You can add custom buttons to the top bar
typedef void(^ButtonTappedBlock)(SimplePickerSheet *picker, UIBarButtonItem *button);



@interface SimplePickerSheet : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverControllerDelegate>
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, copy) SimpleCompletionBlock completionBlock;

@property (nonatomic, retain)NSString *selectedItem;

//------------------------
// Use these to create the popover
-(id)initWithTitle:(NSString*)title andCancelButton:(BOOL)showCancelButton dismissed:(SimpleCompletionBlock)completionBlock;
-(void)addComponent:(NSArray*)rowValues  initialSelection:(int)initialSelection changed:(ComponentChangedBlock)changedActionBlock;

-(void)showInView:(UIView*)view;


@end
